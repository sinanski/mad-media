import React, { PureComponent } from 'react';
import DragNDrop from "../../core/draggable";
import Contract from "../../constructors/contracts/Contract";
import styled from "styled-components";
import Movie from "./Movie";
import { Ad } from "../../constructors/contracts/AdInterface";

interface Props {
  ads_owned: Array<Ad>
  ads_in_Program: Array<Ad>
}

const Storage = styled.div`
  background-color: lightblue;
  width: 200px;
  margin: 10px;
`;

const Storage2 = styled.div`
  background-color: palevioletred;
  width: 200px;
  margin: 10px;
`;

function getElementData(amount = 3): Array<Ad> {
  const commercials = new Contract();
  return commercials.random( amount );
}

function getColumnData(key: string, commercials?: Array<Ad>) {
  const list = commercials || [];
  const elementIds = list.map( val => val.id );
  return {
    [key]: {
      id: key,
      title: key,
      elementIds
    },
  }
}

class ProgramPlanner extends PureComponent<Props> {

  static defaultProps = {
    ads_owned: getElementData( 6 ),
    ads_in_Program: getElementData( 2 ),
  };

  render() {
    const { ads_owned, ads_in_Program } = this.props;
    const commercials = [ ...ads_owned, ...ads_in_Program ];
    const columnData = {
      ...getColumnData( 'Werbung', ads_owned ),
      ...getColumnData( 'Programm', ads_in_Program )
    };
    return (
      <div>
        <DragNDrop
          ColumnNode={ [ Storage, Storage2 ] }
          columnData={ columnData }
          ElementNode={ Movie }
          elementData={ commercials }
          onDragEnd={ (result, state) => console.log( result ) }
        />
      </div>
    );
  }
}

export default ProgramPlanner;
