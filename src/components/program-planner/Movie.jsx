import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  margin: 8px;
  font-size: 0.8rem;
  border-radius: 2px;
  height: 45px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 4px 7px;
  background-color: ${ props => props.isDragging ? '#ffffffbf' : 'white' };
`;

const Movie = ({
  title,
  isDragging
}) => (
  <Container isDragging={ isDragging }>
    { title.de }
  </Container>
);

export default Movie
