export const config = {
  show_ui: false,
  character_speed: 1.2,
  character_enter_room_delay: 50,
  elevator_speed: 2,
  elevator_door_speed: 2,
  elevator_delay: 2,
  npcs: true,
};
