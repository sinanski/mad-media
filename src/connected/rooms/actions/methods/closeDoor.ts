import { Store } from "use-global-hook";
import { Actions, State } from "../types";

export const closeDoor = (
  store: Store<State, Actions>,
  id: string,
) => {
  store.setState({
    ...store.state,
      [id]: {
        ...store.state[id],
        is_open: false,
      }
  });
};
