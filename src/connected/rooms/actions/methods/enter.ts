import { Store } from "use-global-hook";
import { Actions, State } from "../types";

export const enter = (
  store: Store<State, Actions>,
  id: string,
) => {
  console.log('enter room', id)
  store.setState({
    ...store.state,
      [id]: {
        ...store.state[id],
        is_occupied: true,
        is_open: true,
      }
  });
};
