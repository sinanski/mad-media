import { Store } from "use-global-hook";
import { Actions, State } from "../types";

export const leave = (
  store: Store<State, Actions>,
  id: string,
) => {
  store.setState({
    ...store.state,
      [id]: {
        ...store.state[id],
        is_occupied: false,
        is_open: true,
      }
  });
};
