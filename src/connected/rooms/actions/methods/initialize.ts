import { Store } from "use-global-hook";
import { Actions, State } from "../types";
import {rooms} from "../../../../views/tower/rooms/rooms";
import { setRooms } from "./helper/setRooms";

export type InitialState = typeof initialState;

export const initialState = {
  id: '',
  is_occupied: false,
  is_rent: false,
  is_owned: false,
  is_valid_for: [],
  is_open: false
};

export const initialize = (
  store: Store<State, Actions>,
) => {
  store.setState( setRooms(rooms, initialState) );
};
