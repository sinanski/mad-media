import { initialize, initialState } from "../initialize";
import { setRooms } from "../helper/setRooms";
import { rooms } from "../../../../../views/tower/rooms/rooms";

jest.mock( '../helper/setRooms' );

const store = {
  setState: jest.fn()
};

describe( 'useRooms.initialize()', () => {
  it( 'should call setRooms()', function () {
    initialize( store );

    expect( setRooms ).toHaveBeenCalledWith( rooms, initialState )
  } );

  it( 'should call setState', function () {
    setRooms.mockReturnValue( 'mock rooms' );
    initialize( store );

    expect( store.setState ).toHaveBeenCalledWith( 'mock rooms' );
  } );
} );
