import { RoomListInterface } from "../../types";
import { Rooms } from "../../../../../views/tower/rooms/rooms";
import { initializeRoom } from "./initializeRoom";
import {initialState as defaultState} from "../initialize";

export const setRooms = (rooms: Rooms, initialState = defaultState): RoomListInterface => {
  let newRooms = {};
  for ( let id in rooms ) {
    newRooms = {
      ...newRooms,
      ...initializeRoom( rooms, id, initialState )
    }
  }

  return newRooms;
};
