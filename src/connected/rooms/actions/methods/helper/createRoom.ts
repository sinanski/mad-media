import {initializeRoom} from "./initializeRoom";
import {initialState} from "../initialize";
import {rooms} from "../../../../../views/tower/rooms/rooms";

export const createRoom = (id: string) => initializeRoom( rooms, id, initialState );
