export const validateCharacter = (id: string) => {

  if ( id.includes( '_mad' ) ) {
    return [ 'mad' ]
  }

  if ( id.includes( '_sun' ) ) {
    return [ 'sun' ]
  }

  if ( id.includes( '_fun' ) ) {
    return [ 'fun' ]
  }

  return [ 'mad', 'sun', 'fun' ]
};
