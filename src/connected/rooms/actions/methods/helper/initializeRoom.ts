import { Rooms } from "../../../../../views/tower/rooms/rooms";
import { validateCharacter } from "./validateCharacter";
import { InitialState } from "../initialize";
import { getPosition } from "../../../room_constructor/methods/getPosition";


export const initializeRoom = (rooms: Rooms, id: string, initialState: InitialState) => ({
  [id]: {
    ...initialState,
    ...getPosition(id, rooms),
    is_valid_for: validateCharacter( id ),
    id
  }
});
