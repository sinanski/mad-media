import {setRooms} from "../setRooms";
import {initializeRoom} from "../initializeRoom";

jest.mock('../initializeRoom');

const mockRooms = {
  mock: {
    room: 'mock room'
  },
  mock2: {
    room2: 'mock room2'
  }
};

const initialState = {
  state: 'mock initial state'
};

describe('setRooms()', () => {

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it( 'should call initializeRoom', function () {
    setRooms(mockRooms, initialState);

    expect(initializeRoom).toHaveBeenCalledWith(
      mockRooms,
      "mock",
      initialState
      );
  } );

  it( 'should return rooms merged with initialState', function () {
    initializeRoom.mockReturnValue(mockRooms)
    const result = setRooms(mockRooms, initialState);

    expect(result).toEqual(mockRooms);
  } );
});
