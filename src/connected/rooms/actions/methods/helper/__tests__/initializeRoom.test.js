import {initializeRoom} from "../initializeRoom";
import { validateCharacter } from "../validateCharacter";
import { getPosition } from "../../../../room_constructor/methods/getPosition";

jest.mock('../validateCharacter');
jest.mock('../../../../room_constructor/methods/getPosition');

const mockRooms = {
  mock: {
    room: 'mock room'
  }
};

const initialState = {
  state: 'mock initial state'
};

const mockPosition = {
  floor: 'mock floor',
  x: 'mock x',
};

const mockId = 'mock';

describe('initializeRoom()', () => {
  it( 'should call validateCharacter()', function () {
    initializeRoom(mockRooms, mockId, initialState);

    expect(validateCharacter).toHaveBeenCalledWith(mockId)
  } );

  it( 'should call getPosition()', function () {
    initializeRoom(mockRooms, mockId, initialState);

    expect(getPosition).toHaveBeenCalledWith(mockId, mockRooms)
  } );

  it( 'should return room object', function () {
    validateCharacter.mockReturnValue('valid characters');
    getPosition.mockReturnValue(mockPosition);
    const room = initializeRoom(mockRooms, mockId, initialState);
    const expected = {
      "mock": {
        ...mockPosition,
        "id": "mock",
        "is_valid_for": "valid characters",
        "state": "mock initial state"
      }
    };

    expect(room).toEqual(expected)
  } );
});
