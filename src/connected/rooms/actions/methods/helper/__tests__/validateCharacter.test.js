import {validateCharacter} from "../validateCharacter";

describe('validateCharacter()', () => {
  it( 'should return all if name does not contain _mad | _sun | _fun', function () {
    const all = validateCharacter('mock name');

    expect(all).toEqual([ 'mad', 'sun', 'fun' ])
  } );

  it( 'should return [mad] if name does contain _mad', function () {
    const mad = validateCharacter('mock_mad');

    expect(mad).toEqual([ 'mad' ])
  } );

  it( 'should return [sun] if name does contain _sun', function () {
    const sun = validateCharacter('mock_sun');

    expect(sun).toEqual([ 'sun' ])
  } );

  it( 'should return [fun] if name does contain _fun', function () {
    const fun = validateCharacter('mock_fun');

    expect(fun).toEqual([ 'fun' ])
  } );
});
