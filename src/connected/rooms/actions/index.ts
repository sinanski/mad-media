import { initialize } from "./methods/initialize";
import { enter } from "./methods/enter";
import { closeDoor } from "./methods/closeDoor";
import { leave } from "./methods/leave";

export default {
  initialize,
  enter,
  leave,
  closeDoor
}
