import { Room, Rooms } from "../../../views/tower/rooms/rooms";
import { RoomType } from "../room_constructor/RoomConstructor";

export interface RoomInterface extends Room {
  id: string;
  is_occupied: boolean;
  is_rent: boolean;
  is_owned: boolean;
  is_valid_for: Array<string>;
  is_open: boolean;
  floor: number;
  x: number;
}

// export type RoomInterface = RoomType

export type RoomListInterface = {
  [index: string]: RoomInterface
}

export type State = RoomListInterface;

export interface Actions {
  initialize: () => void;
  enter: (room_id: string) => void;
  leave: (room_id: string) => void;
  closeDoor: (room_id: string) => void;
}
