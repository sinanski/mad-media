import React from "react";
import useGlobalHook from "use-global-hook";
import { State, Actions } from "../actions/types";
import {rooms} from "../../../views/tower/rooms/rooms";
import actions from '../actions';
import { setRooms } from "../actions/methods/helper/setRooms";
import { initialState as defaultState } from "../actions/methods/initialize";

const initialState = setRooms(rooms, defaultState)

export const useRooms = useGlobalHook<State, Actions>(React, initialState, actions);

export default useRooms;
