import { Room as OldRoom, rooms, Rooms } from "../../../views/tower/rooms/rooms";
import { setRooms } from "../actions/methods/helper/setRooms";
import { validRooms } from "./methods/validRooms";
import { getPosition } from "./methods/getPosition";
import { createTiles } from "./methods/createTiles";

export const tiles_per_floor = 8;
export const tile_width = 80;


export type DecorationType = {
  floor: number;
  hasInteraction: boolean;
  image: string;
  name: string;
  tile: number;
}

export interface RoomType extends DecorationType {
  door: number;
  id: string;
  image_open: string;
  is_occupied: boolean;
  is_open: boolean;
  is_owned: boolean;
  is_rent: boolean;
  is_valid_for: Array<string>;
  x: number;
}

export type RoomOrDecoration = RoomType | DecorationType;

export type Floor = Array<RoomOrDecoration | 0>;

export type Style = { [index: string]: string };

// export interface TileInterface {
//   name: string;
//   image: string;
//   hasInteraction: boolean;
//   style?: Style
// }

export type TileList = Floor;

export type RoomConstruct = RoomConstructor;

type Get = (id: string) => OldRoom;

export class RoomConstructor {
  list: Array<TileList>;
  rooms: Rooms;
  get: Get;
  canEnter: (characterId: string, roomId: string) => boolean;
  validRooms: (characterId: string) => Array<string>;

  constructor() {
    this.list = createTiles( rooms ); //.slice().reverse();
    this.rooms = setRooms( rooms );
    this.get = (id) => getPosition( id, this.rooms );
    this.canEnter = (characterId, roomId) => canEnter( characterId, roomId, this.get );
    this.validRooms = (characterId) => validRooms( characterId, setRooms( this.rooms ) )
    // console.log(this.list)
    // console.log(this.rooms)
    // console.log(this.getPosition(x[0]))

    // this.list = mapFloors(tower_rooms, tiles_per_floor)
  }
}

const canEnter = (
  characterId: string,
  roomId: string,
  get: Get
) => {
  // const room = get(roomId);
  return true
  // return filterValidRooms(room, characterId);
};

