import {getDoor} from "./getDoor";

const mock_no_door = {
  mock_id: {
  },
};

const mock_door = {
  mock_id: {
    door: 2,
  },
};

describe('getDoor()', () => {
  it( 'should return a door', () => {
    const empty = getDoor('mock_id', mock_no_door);
    const door1 = getDoor('mock_id', mock_door);

    expect(empty).toBe(0)
    expect(door1).toBe(1)
  } );
});
