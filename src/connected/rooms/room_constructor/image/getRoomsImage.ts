import { Rooms } from "../../../../views/tower/rooms/rooms";
import door1 from "../../../../assets/img/tower/door1.png";
import door2 from "../../../../assets/img/tower/door2.png";
import door3 from "../../../../assets/img/tower/door3.png";
import door1_open from "../../../../assets/img/tower/door1_open.png";
import door2_open from "../../../../assets/img/tower/door2_open.png";
import door3_open from "../../../../assets/img/tower/door3_open.png";
import { getImage } from "./getImage";
import { getDoor } from "./getDoor";

const doors = [
  door1,
  door2,
  door3,
];

const doors_open = [
  door1_open,
  door2_open,
  door3_open,
];

// todo update tests

export const getRoomsImage = (id: string | 0, rooms: Rooms): string => {

  const decoration = getImage( id, rooms );
  if ( decoration ) {
    return decoration;
  }

  const doorIndex = getDoor( id, rooms );
  return doors[doorIndex]
};

export const getRoomsOpenImage = (id: string | 0, rooms: Rooms): string => {
  const decoration = getImage( id, rooms );
  if ( decoration ) {
    return decoration;
  }

  const doorIndex = getDoor( id, rooms );
  return doors_open[doorIndex]
};
