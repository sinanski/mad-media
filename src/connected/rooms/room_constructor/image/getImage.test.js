import { getImage } from "./getImage";

const mockRooms = {
  mock_room: {
    img: 'mock image'
  },
};

describe('getImage()', () => {
  it( 'should return an image when right id is given', () => {
    const image = getImage('mock_room', mockRooms);

    expect(image).toBe('mock image')
  } );
});
