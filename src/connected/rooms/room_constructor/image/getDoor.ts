import { Rooms } from "../../../../views/tower/rooms/rooms";

export const getDoor = (id: string | 0, rooms: Rooms): number => {
  const index = rooms[id].door || 1;
  return index - 1
};
