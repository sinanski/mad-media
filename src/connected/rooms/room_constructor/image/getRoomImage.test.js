import {getRoomsImage} from "./getRoomsImage";
import { getImage } from "./getImage";
import { getDoor } from "./getDoor";

jest.mock('./getImage', () => ({
  getImage: jest.fn(() => ({img:'test'}))
}));

jest.mock('./getDoor', () => ({
  getDoor: jest.fn(() => (0))
}));

const mockRooms = {
  betty: {
    name: 'Bettys Büro',
    img: 'mock image'
  },
};

describe('getRoomsImage()', () => {

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it( 'should call getImage if string is provided', () => {
    getRoomsImage('mock_id', mockRooms);

    expect(getImage).toHaveBeenCalledWith('mock_id', mockRooms)
  } );

  it( 'should not call getImage if no string is provided', () => {
    getRoomsImage(null, mockRooms);

    expect(getImage).not.toHaveBeenCalled()
  } );

  it( 'should return the right image', () => {
    const image = getRoomsImage('mock_id', mockRooms);

    expect(image).toMatchObject({img:'test'});
  } );

  describe('should call doors', () => {
    it( 'not if no string given',  () => {
      getRoomsImage(null, mockRooms);

      expect(getDoor).not.toHaveBeenCalled()
    } );

    it( 'not if it is decoration',  () => {
      getRoomsImage('betty', mockRooms);

      expect(getDoor).not.toHaveBeenCalled()
    } );

    it( 'when string is provided and no image matches it',  () => {
      getImage.mockReturnValue(false);
      getRoomsImage('mock_empty', mockRooms);

      expect(getDoor).toHaveBeenCalledWith('mock_empty', mockRooms)
    } );

    it( 'and return a door image',  () => {
      getImage.mockReturnValue(false);
      const image = getRoomsImage('mock_empty', mockRooms);

      expect(image).toBe('door1.png')
    } );
  })
});

