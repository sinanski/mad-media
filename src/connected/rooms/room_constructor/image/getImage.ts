import { Rooms } from "../../../../views/tower/rooms/rooms";

export function getImage(id: string | 0, rooms: Rooms): string | undefined {
  return rooms[id].img
}
