import { TileList } from "../RoomConstructor";

export const mapFloors = (content: TileList, tiles_per_floor: number) => {
  let floors: Array<TileList> = [];
  let onFloor: TileList = [];
  content.forEach( (entry, i) => {
    const closeFoor = i % tiles_per_floor === tiles_per_floor - 1;
    onFloor.push( entry );
    if ( closeFoor ) {
      floors.push( onFloor );
      onFloor = [];
    }
  } );
  return floors
};
