import {mapFloors} from "./mapFloors";
import {tower_rooms} from "../../../../views/tower/tower";

describe('mapFloors()', () => {
  it( 'find() should return room', () => {
    const floors = mapFloors(tower_rooms, 8);
    const first_floor = tower_rooms.slice(0,8);

    expect(floors).toHaveLength(13);
    expect(floors[0]).toEqual(first_floor);
  } );
});
