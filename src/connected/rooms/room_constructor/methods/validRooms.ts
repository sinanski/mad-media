import { RoomListInterface } from "../../actions/types";
import { filterValidRooms } from "./filterValidRooms";

export const validRooms = (characterId: string, rooms: RoomListInterface): Array<string> => {

  return Object.values( rooms )
    .filter( (room) => filterValidRooms( room, characterId ) )
    .map( ({ id }) => id );
};
