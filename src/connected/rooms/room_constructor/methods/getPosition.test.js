import { getPosition } from "./getPosition";
import { getFloor } from "./helper/getFloor";
import { getX } from "./helper/getX";
import { getTileIndex } from "./helper/getTileIndex";

jest.mock( './helper/getFloor' );
jest.mock( './helper/getX' );
jest.mock( './helper/getTileIndex' );

const mockId = 'mock id';

const mockRooms = {
  [mockId]: {
    id: 'mock 1 id'
  },
  mock2: {
    id: 'mock 2 id'
  },
};


describe( 'RoomConstructor.getPosition()', () => {
  it( 'should call getFloor()', function () {
    getPosition( mockId, mockRooms );

    expect( getFloor ).toHaveBeenCalledWith( mockId )
  } );

  it( 'should call getX()', function () {
    getPosition( mockId, mockRooms );

    expect( getX ).toHaveBeenCalledWith( mockId )
  } );

  it( 'should call getTileIndex()', function () {
    getPosition( mockId, mockRooms );

    expect( getTileIndex ).toHaveBeenCalledWith( mockId )
  } );

  it( 'should return right object', function () {
    getFloor.mockReturnValue( 'mock floor' );
    getX.mockReturnValue( 'mock x' );
    getTileIndex.mockReturnValue( 'mock tile index' );
    const result = getPosition( mockId, mockRooms );
    const expected = {
      "floor": "mock floor",
      "tile": "mock tile index",
      "id": "mock 1 id",
      "x": "mock x"
    };

    expect( result ).toEqual( expected )
  } );
} );
