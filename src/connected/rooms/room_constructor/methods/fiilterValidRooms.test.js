import { filterValidRooms } from "./filterValidRooms";

const mockRoom = {
  is_valid_for: ['mad', 'sun']
};

describe( 'filterValidRooms', () => {
  it( 'should return a boolean if character is allowed to enter room', function () {
    const result1 = filterValidRooms(mockRoom, 'mad');
    const result2 = filterValidRooms(mockRoom, 'sun');
    const result3 = filterValidRooms(mockRoom, 'fun');

    expect(result1).toBeTruthy();
    expect(result2).toBeTruthy();
    expect(result3).toBeFalsy();
  } );
} );
