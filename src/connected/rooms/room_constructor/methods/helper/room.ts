import { getRoomsImage, getRoomsOpenImage } from "../../image/getRoomsImage";
import { rooms } from "../../../../../views/tower/rooms/rooms";
import { createRoom } from "../../../actions/methods/helper/createRoom";

export const room = (tile: string | 0) => {
  if ( !tile ) {
    return 0;
  }
  return {
    ...createRoom(tile)[tile],
    name: tile,
    image: getRoomsImage( tile, rooms ),
    image_open: getRoomsOpenImage( tile, rooms ),
    hasInteraction: true,
  }
};
