import { tile_width } from "../../RoomConstructor";
import { getTileIndex } from "./getTileIndex";

export const getX = (id: string) => {
  const width = getTileIndex(id) * tile_width;
  return width + tile_width / 2;
};
