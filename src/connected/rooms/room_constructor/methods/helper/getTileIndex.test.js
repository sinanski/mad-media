import { getTileIndex } from "./getTileIndex";
import { tiles_per_floor } from "../../RoomConstructor";


const mockRooms = [
  0, 0, 0, 'mock room', 'mock room +1', 0
];

describe( 'getTileIndex', () => {
  it( 'should return index of room or index +1 if room is right of elevator (index > 3)', function () {
    const index = getTileIndex( 'mock room', mockRooms );
    const indexPlusOne = getTileIndex( 'mock room +1', mockRooms );
    const noMatch = getTileIndex( 'no match', mockRooms );

    expect( index ).toBe( 3 );
    expect( indexPlusOne ).toBe( 5 );
    expect( noMatch ).toBe( -1 );
  } );

  it( 'tiles_perFloor should be 8', function () {
    expect(tiles_per_floor).toBe(8);
  } );
} );