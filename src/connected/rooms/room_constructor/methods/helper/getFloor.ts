import { tower_rooms } from "../../../../../views/tower/tower";
import { tiles_per_floor } from "../../RoomConstructor";

// rooms is only asignable for testing
export const getFloor = (id: string, rooms = tower_rooms): number => {
  const index = rooms.indexOf( id );
  const floor = Math.floor( index / tiles_per_floor );
  return 12 - floor
};
