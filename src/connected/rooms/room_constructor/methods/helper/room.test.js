import { room } from "./room";
import { getRoomsImage } from "../../image/getRoomsImage";
import { rooms } from "../../../../../views/tower/rooms/rooms";
import { createRoom } from "../../../actions/methods/helper/createRoom";

jest.mock( '../../image/getRoomsImage' );
jest.mock( '../../../actions/methods/helper/createRoom' );

describe( 'room()', () => {

  beforeEach( () => {
    jest.clearAllMocks();
  } );

  it( 'should call createRoom()', function () {
    createRoom.mockReturnValue({mock: 'mock val'});
    room('mock');

    expect(createRoom).toHaveBeenCalledWith('mock');
  } );

  it( 'should return 0 if no tile is provided', function () {
    const result = room();

    expect( result ).toBe( 0 );
    expect( getRoomsImage ).not.toHaveBeenCalled();
  } );

  it( 'should return the right room if tile is provided', function () {
    const mockObj = {key: 'mock val'};
    getRoomsImage.mockReturnValue( 'mock image' );
    createRoom.mockReturnValue({'mock tile': mockObj});
    const mockTile = 'mock tile';
    const result = room( mockTile );
    const expected = {
      ...mockObj,
      "hasInteraction": true,
      "image": "mock image",
      "name": mockTile
    };

    expect( result ).toEqual( expected )
    expect( getRoomsImage ).toHaveBeenCalledWith( mockTile, rooms );
  } );
} );
