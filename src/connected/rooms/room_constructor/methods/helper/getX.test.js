import { getX } from "./getX";
import {getTileIndex} from "./getTileIndex";
import { tile_width } from "../../RoomConstructor";

jest.mock('./getTileIndex');

const mockId = 'mock id';
const mockIndex = 5;

describe('getX', () => {
  it( 'should call getTileIndex', function () {
    getX(mockId);

    expect(getTileIndex).toHaveBeenCalledWith(mockId)
  } );

  it( 'should return index * tile_width + tile_width / 2', function () {
    getTileIndex.mockReturnValue(mockIndex);
    const result = getX(mockId);
    const expected = mockIndex * tile_width + tile_width / 2;

    expect(result).toBe(expected);
    expect(tile_width).toBe(80);
  } );
});
