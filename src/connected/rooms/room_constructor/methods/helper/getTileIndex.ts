import { tower_rooms } from "../../../../../views/tower/tower";
import { tiles_per_floor } from "../../RoomConstructor";

// rooms can only be provided for testing purpose
export const getTileIndex = (id: string, rooms = tower_rooms): number => {
  const index = rooms.indexOf( id );
  const tile: number = index % tiles_per_floor;
  return tile < 4 ? tile : tile + 1;
};
