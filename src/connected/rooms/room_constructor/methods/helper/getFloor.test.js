import {getFloor} from "./getFloor";

const id1 = 'mock first floor';
const id2 = 'mock second floor';

const mockRooms = [
  0,0,0,id1, 0,0,0,0,id2
];

describe('getFloor()', () => {
  it( 'should return 12 - (index rounded down)', function () {
    // The array of the rooms is upside down to match human eyes. This reverts it
    const floor12 = getFloor(id1, mockRooms);
    const floor11 = getFloor(id2, mockRooms);

    expect(floor12).toBe(12);
    expect(floor11).toBe(11);
  } );
});
