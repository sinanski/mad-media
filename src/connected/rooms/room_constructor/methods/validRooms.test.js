import {validRooms} from "./validRooms";
import {filterValidRooms} from "./filterValidRooms";

jest.mock('./filterValidRooms');

const mockRooms = {
  mock1: {
    id: 'mock 1 id'
  },
  mock2: {
    id: 'mock 2 id'
  },
};

const mockId = 'mock character';

describe('validRooms()', () => {
  it( 'should call filterValidRooms', function () {
    validRooms(mockId, mockRooms);

    expect(filterValidRooms).toBeCalledWith(mockRooms.mock1, mockId);
  } );

  it( 'should call filterValidRooms', function () {
    filterValidRooms.mockReturnValue(mockRooms)
    const result = validRooms(mockId, mockRooms);
    const expected = ['mock 1 id', 'mock 2 id']

    expect(result).toEqual(expected)
  } );
});
