import { Rooms } from "../../../../views/tower/rooms/rooms";
import { getFloor } from "./helper/getFloor";
import { getX } from "./helper/getX";
import { getTileIndex } from "./helper/getTileIndex";

export const getPosition = (id: string, rooms: Rooms) => {
  const floor = getFloor( id );
  const x = getX( id );
  const tile = getTileIndex( id );

  return {
    ...rooms[id],
    floor,
    tile,
    x,
  }
};
