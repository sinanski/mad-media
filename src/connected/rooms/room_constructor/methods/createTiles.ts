import { images } from "../../../../views/tower/rooms/images";
import { decorations, tower_rooms } from "../../../../views/tower/tower";
import { room } from "./helper/room";
import { mapFloors } from "./mapFloors";
import { TileList, tiles_per_floor } from "../RoomConstructor";
import { Rooms } from "../../../../views/tower/rooms/rooms";

type Possible = string | 0;

export const createTiles = (rooms: Rooms) => {
  const decoration = createDeco();
  const doors = createDoor();
  const merged = mergeLists( decoration, doors );
  return mapFloors( merged, tiles_per_floor )
};

const createDeco = () =>
  decorations.map( (tile, index) => (
    deco( tile, index )
  ) );

const createDoor = () =>
  tower_rooms.map( (tile) => (
    room( tile )
  ) );

const mergeLists = (decoration: TileList, r: TileList) => {
  return r.map( (room, i) => {
    return room ? room : decoration[i]
  } )
};

const deco = (id: Possible, index: number) => {
  const floor = getFloorFromIndex( index );
  const tile = getTileFromIndex( index );

  if ( !id ) {
    return 0;
  }
  return {
    floor,
    tile,
    name: id,
    image: images[id],
    hasInteraction: false
  }
};

const getTileFromIndex = (index: number): number => {
  const tile = index % tiles_per_floor;
  return tile < 4 ? tile : tile + 1;
};

const getFloorFromIndex = (index: number): number => {
  const floor = index / tiles_per_floor;
  return 12 - Math.floor( floor )
};
