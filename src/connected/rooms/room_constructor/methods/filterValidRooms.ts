import { RoomInterface } from "../../actions/types";

export const filterValidRooms = (room: RoomInterface, characterId: string) =>
  room.is_valid_for
    .indexOf( characterId ) !== -1;
