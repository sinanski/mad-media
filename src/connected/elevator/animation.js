import { gsap } from 'gsap';
import { config } from "../../config";

const { elevator_door_speed = 1, elevator_delay = 1 } = config;

const durationDoor = () =>
  1 / elevator_door_speed;

export function openDoor({
  doors,
  onComplete
}) {
  return new gsap
  .timeline( { paused: true } )
  .to( doors.left.current, {
    duration: durationDoor(),
    x: -28
  }, 0 )
  .to( doors.right.current, {
    duration: durationDoor(),
    x: 28,
    onComplete
  }, 0 )
}

export function closeDoor({
  doors,
  onComplete
}) {
  return new gsap
  .timeline( { paused: true } )
  .to( doors.left.current, {
    duration: durationDoor(),
    x: 0
  }, 0 )
  .to( doors.right.current, {
    duration: durationDoor(),
    x: 0,
    onComplete
  }, 0 )
}

export function move(
  domRef,
  target,
  onComplete
) {
  return new gsap
  .timeline( { paused: true } )
  .to( domRef.current, {
    duration: target.time,
    y: target.position,
    ease: 'linear',
    onComplete
  } );
}

export function moveIndicator(
  domRef,
  x,
  duration
) {
  return new gsap
  .timeline( { paused: true } )
  .to( domRef.current, {
    duration,
    x,
    ease: 'linear',
  } );
}

export function ready(onComplete) {
  return new gsap
  .timeline( { paused: true } )
  .to( {}, {
    duration: 1 / elevator_delay,
    onComplete
  } )
}