import React, { useEffect, useRef } from 'react';
import styled from 'styled-components';
import { useElevator } from "./store";
import { moveIndicator } from "./animation";
import { State as Elevator } from "./actions/types";
import {config} from "../../config";

interface Props {
  floor: number
}

const Container = styled.div`
  position:absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0;
  left: 0;
  width: 100%;
  height: 10px;
  z-index: 10;
`;

const Box = styled.div`
  width: 39px;
  height: 3px;
  background-color: white;
`;

const Dot = styled.div`
  height: 100%;
  width: 2px;
  background-color: red;
  position: relative;
  left: 0px;
  //transition: all 1s ease;
`;

const getDelta = (elevator: Elevator) =>
  (0 - elevator.target) * -1;

const deltaX = (elevator: Elevator) => {
  return getDelta(elevator) * 3.1
};

const deltaDuration = (elevator: Elevator) => {
  const duration = elevator.target - elevator.current_floor;
  // const duration = elevator.current_floor - elevator.target;
  const positiveTime = Math.sqrt(Math.pow(duration, 2));
  return positiveTime / config.elevator_speed;
};

const Indicator: React.FC<Props> = ({ floor }) => {
  const [ elevator ] = useElevator();
  const dotRef = useRef( null );

  useEffect( () => {
    if ( elevator.is_moving ) {
      const x = deltaX(elevator);
      const duration = deltaDuration(elevator);
      moveIndicator(
        dotRef,
        x,
        duration
      ).play()
    }
  }, [ elevator ] )
  return (
    <Container>
      <Box>
        <Dot ref={ dotRef } />
      </Box>
    </Container>
  );
}

export default Indicator
