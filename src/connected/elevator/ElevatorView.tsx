import React from 'react';
import { Box, ElevatorBg, ElevatorShaft, ElevatorTile, Frame, BoxBg, BoxCharacter } from "./Styles";
import Door from "./door/Door";
import elevator_open from "../../assets/img/tower/elevator_inner.png";
import elevator_frame from "../../assets/img/tower/elevator_frame.png";
import { Floors } from "../../views/tower/floors/Floors";
import Indicator from "./Indicator";
import { image } from "../character/image";


interface Props {
  floors: Floors;
  elevatorRef: any;
  character?: number;
}

const ElevatorView: React.FC<Props> = (
  {
    floors,
    elevatorRef,
    character
  }
) => (
  <ElevatorShaft id="Elevator">
    { floors.map( (floor, i) => (
      <ElevatorTile key={ i }>
        <ElevatorBg />
        <Frame src={ elevator_frame } />
        <Door floor={ 12 - i } />
        <Indicator floor={ i } />
      </ElevatorTile>
    ) ) }
    <Box ref={ elevatorRef }>
      { character &&
      <BoxCharacter src={ image[character].standing } />
      }
      <BoxBg src={ elevator_open } />
    </Box>
  </ElevatorShaft>
);

export default ElevatorView
