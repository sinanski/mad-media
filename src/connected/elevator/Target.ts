import { State } from "./actions/types";
import { State as Characters } from "../character/actions/types";
import { config } from "../../config";

const { elevator_speed } = config;

export class Target {
  position: number;
  indicator: number;
  time: number;
  next: number;

  constructor(
    private elevator: State,
    private characters: Characters
  ) {
    const next = getNextTarget( elevator, characters );
    const delta = (0 - next) * -1;
    const deltaTime = cleanSign( elevator.current_floor - next );
    this.next = next;
    this.indicator = delta * 3.1;
    this.position = delta * -90;
    this.time = deltaTime / elevator_speed
  }
}

function cleanSign(num: number): number {
  return Math.sqrt(
    Math.pow( num, 2 )
  );
}

const getNextTarget = (elevator: State, characters: Characters): number => {
  const index = elevator.character;
  let target = elevator.called_to[0];
  if ( index ) {
    target = characters[index].target.floor
  }
  return target;
};