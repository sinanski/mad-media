import styled from "styled-components";

export const ElevatorShaft = styled.div`
  position: relative;
  grid-area: elevator;
`;
export const ElevatorTile = styled.div`
  position: relative;
  left: 0;
  width: 80px;
  height: 80px;
  margin-bottom: 10px;
`;

export const ElevatorBg = styled.div`
  position: absolute;
  width: 58px;
  height: 66px;
  bottom: 0;
  left: 11px;
  background: black;
`;

export const Frame = styled.img`
  position:absolute;
  width: 100%;
  height: 100%;
  bottom: 0;
  left: 0;
  z-index: 2;
`;

export const Box = styled.div`
  position:absolute;
  width: 80px;
  height: 80px;
  bottom: 0;
  left: 0;
`;

export const BoxBg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const BoxCharacter = styled.img`
  position:absolute;
  width: 100%;
  height: 60px;
  object-fit: contain;
  bottom: 0;
  left: 0;
`;
