import React from "react";
import useGlobalHook from "use-global-hook";
// todo Shit, importing from different sources.
//  Here is local source, within methods it is global.
//  Gotta fix this!
// import useGlobalHook from "../../assets/functions/use/useGlobal";

import { State, ElevatorActions } from "../actions/types";

// import * as actions from "../actions";
import actions from '../actions'


export const initialState = {
  current_floor: 0,
  target: 0,
  called_to: [],
  called_by: [],
  has_arrived: true,
  is_empty: true,
  is_moving: false,
  is_opening: true,
  is_closing: false,
  is_open: false,
  is_closed: false,
  is_ready: false,
};

export const useElevator = useGlobalHook<State, ElevatorActions>(React, initialState, actions);
