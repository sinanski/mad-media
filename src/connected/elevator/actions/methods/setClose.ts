import { Store } from "use-global-hook";
import { ElevatorActions, State } from "../types";

export const setClose = (
  store: Store<State, ElevatorActions>,
) => {
  store.setState( {
    ...store.state,
    is_closing: false,
    is_closed: true,
  } );
};
