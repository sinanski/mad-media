import { Store } from "use-global-hook";
import { ElevatorActions, State } from "../types";

export const setOpen = (
  store: Store<State, ElevatorActions>,
) => {
  store.setState( {
    ...store.state,
    is_opening: false,
    is_open: true
  } );
};
