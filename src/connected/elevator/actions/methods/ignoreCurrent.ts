import { State } from "../types";

export const ignoreCurrent = (state: State) => {
  const { called_to, current_floor } = state;
  const length = called_to.length - 1;
  return called_to[0] !== current_floor
    ? called_to
    : called_to.slice().splice( 1, length );
};
