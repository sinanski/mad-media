export const store = {
  state: {
    current_floor: 'mock current_floor',
    mock2: 'mock2_val',
    called_to: ['first mock', 'second mock'],
    called_by: ['first mock character', 'second mock character'],
    target: 'mock target'
  },
  setState: jest.fn()
};

const mockStore = {
  ...store,
  state: {
    ...store.state,
    called_to: ['first mock']
  }
};