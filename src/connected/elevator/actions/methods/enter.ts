import { Store } from "use-global-hook";
import { ElevatorActions, State } from "../types";

export const enter = (
  store: Store<State, ElevatorActions>,
  character: number
) => {
  store.setState( {
    ...store.state,
    character,
    is_empty: false
  } );
};
