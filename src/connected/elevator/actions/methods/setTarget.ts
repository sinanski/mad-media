import { Store } from "use-global-hook";
import { ElevatorActions, State } from "../types";

export const setTarget = (
  store: Store<State, ElevatorActions>,
  target_floor: number
) => {
  store.setState( {
    ...store.state,
    target: target_floor,
  } );
};
