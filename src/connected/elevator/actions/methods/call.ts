import { Store } from "use-global-hook";
import { ElevatorActions, Floor, State } from "../types";

export const call = (
  store: Store<State, ElevatorActions>,
  floor: Floor,
  character: number
) => {
  const { called_to, called_by } = store.state;

  // Hier kann in den 13. Stock gecalled werden. Keine Ahnung wie das entsteht.

  const hasAlreadyCalled = called_by.indexOf(character) !== -1;

  if (!hasAlreadyCalled) {

    store.setState( {
      ...store.state,
      called_to: called_to.concat( floor ),
      called_by: called_by.concat( character )
    } )
  }
};
