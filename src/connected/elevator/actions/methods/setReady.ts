import { Store } from "use-global-hook";
import { ElevatorActions, State } from "../types";

export const setReady = (
  store: Store<State, ElevatorActions>
) => {
  store.setState( {
    ...store.state,
    is_ready: true,
  } );
};
