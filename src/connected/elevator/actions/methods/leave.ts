import { Store } from "use-global-hook";
import { ElevatorActions, State } from "../types";
import { ignoreCurrent } from "./ignoreCurrent";

export const leave = (
  store: Store<State, ElevatorActions>,
) => {
  store.setState( {
    ...store.state,
    called_to: ignoreCurrent(store.state),
    has_arrived: false,
    is_closing: true,
    is_open: false,
    is_opening: false,
    is_ready: false,
  } );
};

