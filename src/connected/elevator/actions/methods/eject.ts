import { Store } from "use-global-hook";
import { ElevatorActions, State } from "../types";

export const eject = (
  store: Store<State, ElevatorActions>,
) => {
  const { called_by } = store.state;

  store.setState( {
    ...store.state,
    character: undefined,
    called_by: called_by.slice( 1 ),
    is_empty: true,
  } );
};
