import { Store } from "use-global-hook";
import { ElevatorActions, State } from "../types";

export const arrive = (
  store: Store<State, ElevatorActions>
) => {
  const { target } = store.state;

  store.setState( {
    ...store.state,
    current_floor: target,
    has_arrived: true,
    is_moving: false,
    is_opening: true,
    is_closed: false,
  } );
};
