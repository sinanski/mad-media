import { enter } from "../enter";
import { store } from "../mockStore";

describe('enter()', () => {
  it( 'should return elevator with entered character index', function () {
    const character = 'mock character';
    enter(store, character);
    const expected = {
      ...store.state,
      character,
      is_empty: false
    };

    expect(store.setState).toHaveBeenCalledWith(expected);
  } );
});
