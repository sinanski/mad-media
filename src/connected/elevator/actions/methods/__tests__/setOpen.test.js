import { setOpen } from "../setOpen";
import { store } from "../mockStore";

describe('setOpen()', () => {
  it( 'should return elevator with entered character index', function () {
    setOpen(store);
    const expected = {
      ...store.state,
      is_opening: false,
      is_open: true
    };

    expect(store.setState).toHaveBeenCalledWith(expected);
  } );
});
