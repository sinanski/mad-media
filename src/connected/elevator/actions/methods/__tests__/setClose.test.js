import { setClose } from "../setClose";
import { store } from "../mockStore";

describe('setClose()', () => {
  it( 'should return elevator with entered character index', function () {
    setClose(store);
    const expected = {
      ...store.state,
      is_closing: false,
      is_closed: true,
    };

    expect(store.setState).toHaveBeenCalledWith(expected);
  } );
});
