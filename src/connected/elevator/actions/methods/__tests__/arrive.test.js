import { arrive } from "../arrive";
import { store } from "../mockStore";

describe('arrive()', () => {
  it( 'should slice the first from caaled_to and return new position', function () {
    arrive(store, 'third mock');
    const expected = {
      ...store.state,
      called_to: [
        "first mock",
        "second mock",
      ],
      current_floor: 'mock target',
      has_arrived: true,
      is_moving: false,
      is_opening: true,
      is_closed: false,
    };

    expect(store.setState).toHaveBeenCalledWith(expected);
  } );
});
