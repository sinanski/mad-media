import {eject} from "../eject";
import {store} from "../mockStore";

describe('eject()', () => {
  it( 'should return character and is_empty to initial value', function () {
    eject(store);
    const expected = {
      ...store.state,
      called_by: [
        "second mock character",
      ],
      character: undefined,
      is_empty: true,
    };

    expect(store.setState).toHaveBeenCalledWith(expected);
  } );
});
