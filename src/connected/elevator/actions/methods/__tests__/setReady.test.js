import { setReady } from "../setReady";
import { store } from "../mockStore";

describe('setReady()', () => {
  it( 'should set is_ready to true', function () {
    setReady(store);
    const expected = {
      ...store.state,
      is_ready: true
    };

    expect(store.setState).toHaveBeenCalledWith(expected);
  } );
});
