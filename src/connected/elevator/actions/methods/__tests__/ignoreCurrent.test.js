import { ignoreCurrent } from "../ignoreCurrent";
import { store } from "../mockStore";

describe('ignoreCurrent()', () => {
  it( 'should return the called_to array if elevator is not in the called floor', function () {
    const mockCall = ignoreCurrent(store.state);

    expect(mockCall).toMatchObject(store.state.called_to)
  } );

  it( 'should return only target floor if elevator is at the same floor as character', function () {
    const mockState = {
      ...store.state,
      current_floor: 'first mock'
    };
    const mockCall = ignoreCurrent(mockState);

    expect(mockCall).toEqual(['second mock'])
  } );
});
