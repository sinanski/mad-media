import { leave } from "../leave";
import { store } from "../mockStore";
import { ignoreCurrent } from "../ignoreCurrent";

jest.mock('../ignoreCurrent', () => ({
  ignoreCurrent: jest.fn(val => 'called_to mock')
}));

describe( 'leave()', () => {
  it( 'should return new position', function () {
    leave( store );
    const expected = {
      ...store.state,
      called_to: 'called_to mock',
      has_arrived: false,
      is_closing: true,
      is_open: false,
      is_opening: false,
      is_ready: false,
    };

    expect( ignoreCurrent ).toHaveBeenCalledWith( store.state );
    expect( store.setState ).toHaveBeenCalledWith( expected );
  } );
} );
