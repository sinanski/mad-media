import { call } from "../call";
import { store } from "../mockStore";

describe('call()', () => {
  it( 'should add the called floor to the array of called_to', function () {
    call(store, 'third mock', 'third mock character');
    const expected = {
      ...store.state,
      called_to: [
        "first mock",
        "second mock",
        "third mock",
      ],
      called_by: [
          "first mock character",
          "second mock character",
          'third mock character',
        ],
    };

    expect(store.setState).toHaveBeenCalledWith(expected);
  } );
});
