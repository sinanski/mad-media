import { setMoving } from "../setMoving";
import { store } from "../mockStore";

describe( 'setMoving()', () => {
  it( 'should return elevator with entered character index', function () {
    setMoving( store );
    const expected = {
      ...store.state,
      is_moving: true
    };

    expect( store.setState ).toHaveBeenCalledWith( expected );
  } );
} );
