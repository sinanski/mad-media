export type State = {
  current_floor: number;
  target: number;
  called_to: Array<number>;
  called_by: Array<number>;
  has_arrived: boolean;
  character?: number;
  is_empty: boolean;
  is_moving: boolean;
  is_opening: boolean;
  is_closing: boolean;
  is_open: boolean
  is_closed: boolean;
  is_ready: boolean;
}

export type Floor = number | Array<number>

export type Call = (floor: Floor, character: number) => void;

export type ElevatorActions = {
  call: Call;
  arrive: () => void;
  leave: () => void;
  setMoving: () => void;
  setOpen: () => void;
  setClose: () => void;
  setReady: () => void;
  enter: (character: number) => void;
  eject: () => void;
  setTarget: (target_floor: number) => void;
}
