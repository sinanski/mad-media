import { setReady } from "./methods/setReady";
import { eject } from "./methods/eject";
import { enter } from "./methods/enter";
import { setOpen } from "./methods/setOpen";
import { setClose } from "./methods/setClose";
import { setMoving } from "./methods/setMoving";
import { call } from "./methods/call";
import { arrive } from "./methods/arrive";
import { leave } from "./methods/leave";
import { setTarget } from "./methods/setTarget";

export default {
  call,
  arrive,
  setMoving,
  leave,
  setClose,
  setOpen,
  enter,
  eject,
  setReady,
  setTarget
}
