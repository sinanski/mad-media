import React, { useEffect, useRef } from "react";
import { Floors } from "../../views/tower/floors/Floors";
import { useElevator } from "./store";
import ElevatorView from "./ElevatorView";
import { move } from "./animation";
import { State } from "./actions/types";
import useCharacter from "../character/store";
import { Target } from "./Target";

interface Props {
  floors: Floors
}

const isReadyToLeave = (elevator: State) => {
  return elevator.called_to.length
    && elevator.has_arrived
    && elevator.is_ready
};

const Elevator: React.FC<Props> = ({ floors }) => {
  const [ get, set ] = useElevator();
  const [ allCharacters ] = useCharacter();
  const elevatorBox = useRef( null );

  useEffect( () => {
    const readyToLeave = isReadyToLeave( get );
    const shouldLeave = get.is_closed && !get.is_moving;

    const moveElevator = () => {
      const target = new Target( get, allCharacters );
      set.setTarget( target.next );

      return move(
        elevatorBox,
        target,
        set.arrive
      );
    };

    if ( readyToLeave ) {
      set.leave();
    }

    if ( shouldLeave ) {
      set.setMoving();

      moveElevator().play();
    }

  }, [ get, set, allCharacters ] );

  return (
    <ElevatorView
      character={ get.character }
      floors={ floors }
      elevatorRef={ elevatorBox }
    />
  );
};

export default Elevator;
