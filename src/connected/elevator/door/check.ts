import { State } from "../actions/types";

export const check = (
  elevator: State,
  floor: number
) => {
  const open = shouldOpen(elevator, floor);
  const close = shouldClose(elevator, floor);
  return {
    open,
    close
  }
};

const currentFloor = (current_floor: number, floor: number) =>
  floor === current_floor;

const shouldOpen = (elevator: State, floor: number) => {
  const {
    current_floor,
    has_arrived,
    is_opening,
    is_open,
    is_ready,
  } = elevator;

  const isCurrentFloor = currentFloor(current_floor, floor);

  return has_arrived
    && isCurrentFloor
    && is_opening
    && !is_open
    && !is_ready
};

const shouldClose = (elevator: State, floor: number) => {
  const {
    current_floor,
    has_arrived,
    is_closing,
  } = elevator;
  const isCurrentFloor = currentFloor(current_floor, floor);
  return !has_arrived && isCurrentFloor && is_closing;
};
