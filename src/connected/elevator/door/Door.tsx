import React, { useEffect, useRef, useState } from 'react';
import { useElevator } from "../store";
import imgLeft from '../../../assets/img/tower/elevator_door_left.png';
import imgRight from '../../../assets/img/tower/elevator_door_right.png';
import * as animate from '../animation';
import {check} from "./check";
import {Container, Image} from "./Styles";

interface Props {
  floor: number;
}

const Door: React.FC<Props> = ({ floor }) => {
  const [ get, set ] = useElevator();
  const [ transitioning, setTransitioning ] = useState(false);

  const doors = {
    left: useRef( null ),
    right: useRef( null )
  };

  useEffect( () => {
    const shouldDoors = check(get, floor);

    const openDoors = () =>
      animate.openDoor({
        doors,
        onComplete: () => {
          set.setOpen();
          setReady();
          setTransitioning(false);
        }
      });

    const closeDoors = () =>
      animate.closeDoor({
        doors,
        onComplete: () => {
          set.setClose();
          setTransitioning(false);
        }
      });

    const setReady = () => {
      animate.ready( set.setReady ).play();
    };

    if ( shouldDoors.open && !transitioning ) {
      setTransitioning(true);
      openDoors().play().delay(.3)
    }

    if ( shouldDoors.close && !transitioning ) {
      setTransitioning(true);
      closeDoors().play();
    }
  }, [
    get,
    set,
    transitioning,
    doors,
    floor
  ] );

  return (
    <Container>
      <Image ref={ doors.left } src={ imgLeft } />
      <Image ref={ doors.right } src={ imgRight } />
    </Container>
  );
};

export default Door;
