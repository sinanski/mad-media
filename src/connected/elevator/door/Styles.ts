import styled from "styled-components";

export const Container = styled.div`
  position: absolute;
  pointer-events: none;
  bottom: 0;
  left: 0;
  width: 80px;
  z-index: 3;
`;

export const Image = styled.img`
position: absolute;
bottom: 0;
left: 0;
  width: 80px;
  height: 80px;
`;
