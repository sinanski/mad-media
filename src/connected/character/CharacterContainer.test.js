import React from 'react';
// import { render } from '@testing-library/react'
// import CharacterAnimator from "./CharacterAnimator";
// import { initialState as initialElevatorState } from "../elevator/store";
// import { initialCharacter } from "./store";

describe( 'CharacterContainer', () => {
  const setState = jest.fn();
  const useRef = jest.fn( (initState) => ({ current: initState }) );
  const useStateMock = (initState) => [ initState, setState ];
  // const useRefMock = (initState) => ({ current: useRef });
  const useRefMock = (initState) => ({ current: initState });

  afterEach( () => {
    jest.clearAllMocks();
  } );

  it( 'should call hooks for character and elevator', () => {
  //   jest.spyOn( React, 'useState' ).mockImplementation( useStateMock );
  //   render( <CharacterAnimator characterIndex={ 1 } /> );
  //   const characterState = setState.mock.calls[0][0];
  //   const elevatorState = setState.mock.calls[1][0];
  //   const expectedCharacterState = {
  //     1: initialCharacter,
  //     2: initialCharacter,
  //     3: initialCharacter,
  //   };
  //
  //   expect( setState ).toHaveBeenCalledTimes( 2 );
  //   expect( characterState ).toMatchObject( expectedCharacterState );
  //   expect( elevatorState ).toMatchObject( initialElevatorState );
  } );
} );