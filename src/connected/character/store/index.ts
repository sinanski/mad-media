import React from "react";
import useGlobalHook from "use-global-hook";
import { State, Actions } from "../actions/types";
// import useGlobalHook from "../../assets/functions/use/useGlobal";
import actions from '../actions'

export const initialCharacter = {
  id: '',
  index: 0,
  is_moving: false,
  has_arrived: false,
  needs_elevator: false,
  in_elevator: false,
  in_room: false,
  way_points: [],
  position: {
    floor: 0,
    x: 0,
  },
  target: {
    floor: 0,
    x: 400
  }
};

const initialState = {
  1: initialCharacter,
  2: initialCharacter,
  3: initialCharacter,
};

export const useCharacter = useGlobalHook<State, Actions>(React, initialState, actions);

export default useCharacter;
