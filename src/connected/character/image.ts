import blueStanding from '../../assets/img/character/character_stand_blue.png';
import blueWalking from '../../assets/img/character/character_walk_blue.gif';
import redStanding from '../../assets/img/character/character_stand_red.png';
import redWalking from '../../assets/img/character/character_walk_red.gif';
import greenStanding from '../../assets/img/character/character_stand_green.png';
import greenWalking from '../../assets/img/character/character_walk_green.gif';

interface Image {
  [index: number]: {
    standing: string;
    walking: string;
  }
}

export const image: Image = {
  1: {
    standing: blueStanding,
    walking: blueWalking
  },
  2: {
    standing: redStanding,
    walking: redWalking
  },
  3: {
    standing: greenStanding,
    walking: greenWalking
  },
};
