import React from 'react';
import styled from 'styled-components';
import { image } from "./image";
import { Character, Position } from "./actions/types";

interface Props {
  position: Position;
  character: Character;
  charRef: any;
  hide: boolean;
  characterIndex: number;
}

interface StyleProps {
  position: Position;
  hide: boolean;
}

const Container = styled.div<StyleProps>`
  visibility: ${ props => props.hide ? 'hidden' : 'visible' };
  pointer-events: none;
  grid-column-start: 1;
  grid-column-end: 10;
  grid-row-end: ${ props => 14 - props.position.floor };
  width: 80px;
  height: 61px;
  position: absolute;
  bottom: -5px;
  z-index: 100;
`;

const Image = styled.img<Character>`
  height: 100%;
  object-fit: contain;
  transform: scaleX(${ props => {
    const target = props.way_points.length && props.way_points[0].x;
    const position = props.position.x +40;
    return target < position ? -1 : 1
  }});
`;

const getImage = (characterIndex: number, isWalking?: boolean,) =>
  isWalking ? image[characterIndex].walking : image[characterIndex].standing;

const Sprite: React.FC<Props> = (
  {
    position,
    character,
    charRef,
    hide,
    characterIndex
  }
) => (
  <Container
    position={ position }
    hide={ hide }
    ref={ charRef }
  >
    <Image
      {...character}
      position={ position }
      src={ getImage( characterIndex, character.is_moving ) }
    />
  </Container>
);

export default Sprite
