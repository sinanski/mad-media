export type State = {
  [index: string]: Character;
}

export type Position = {
  floor: number;
  x: number;
  in_elevator?: boolean;
  id?: string;
  call_elevator?: Array<number>
}

export type PositionUpdate = {
  floor?: number;
  x?: number;
  in_elevator?: boolean;
  id?: string;
  call_elevator?: Array<number>
}

export type WayPoints = Array<Position>;

export type CharacterBase = {
  id: string;
  is_moving: boolean;
  in_room: boolean;
  has_arrived: boolean;
  needs_elevator: boolean;
  in_elevator: boolean;
  // floor?: number;
  // x?: number;
}

export interface Character extends CharacterBase {
  index: number;
  way_points: WayPoints;
  position: Position;
  target: Position;
}

export type Actions = {
  setTarget: (characterIndex: number, target: Position) => void;
  // setPosition: (characterIndex: number, position: Position) => void;
  setX: (characterIndex: number, x: number) => void;
  arriveAtFloor: (characterIndex: number, position: Position) => void;
  arrive: (characterIndex: number, position: Position) => void;
  leaveRoom: () => void;
  enterElevator: (characterIndex: number) => void;
  initialize: (characterIndex: number) => void;
}

export interface NewPosition extends CharacterBase, Position {
  id: string;
  in_elevator: boolean;
}
