import { setTarget } from "./methods/setTarget";
import { arriveAtFloor } from "./methods/arriveAtFloor";
import { arrive } from "./methods/arrive";
import { setX } from "./methods/setX";
import { enterElevator } from "./methods/enterElevator";
import { leaveRoom } from "./methods/leaveRoom";
import { initialize } from "./methods/initialize";

export default {
  setTarget,
  arriveAtFloor,
  arrive,
  setX,
  enterElevator,
  leaveRoom,
  initialize
};
