import { Store } from "use-global-hook";
import { Actions, State } from "../types";
import { getCharacter } from "./helper/getCharacter";
import { getId } from "./helper/getId";

export const initialize = (
  store: Store<State, Actions>,
  characterIndex: number,
) => {
  const character = getCharacter(store.state, characterIndex);
  const id = getId(characterIndex);

  store.setState( {
    ...store.state,
    [characterIndex]: {
      ...character,
      index: characterIndex,
      id
    }
  } );
};

