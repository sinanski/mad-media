import { Store } from "use-global-hook";
import { Actions, PositionUpdate, State } from "../types";
import { getNewPosition } from "./helper/getNewPosition";

export const arrive = (
  store: Store<State, Actions>,
  characterIndex: number,
  position: PositionUpdate
) => {
  const newCharacter = getNewPosition( store, characterIndex, position );

  store.setState( {
    ...store.state,
    [characterIndex]: newCharacter,
  } );
};
