import { Store } from "use-global-hook";
import { Actions, Character, PositionUpdate, State } from "../types";
import { getWayPoints } from "./helper/getWayPoints";
import { needsElevator } from "./helper/needsElevator";
import {getCharacter} from "./helper/getCharacter";

export const setTarget = (
  store: Store<State, Actions>,
  characterIndex: number,
  target: PositionUpdate
) => {
  const character = getCharacter(store.state, characterIndex);
  const newCharacter = getNewCharacter(character, target);

  if ( !character.in_elevator ) {
    store.setState( {
      ...store.state,
      [characterIndex]: newCharacter
    } )
  }
};

const getNewCharacter = (
  character: Character,
  target: PositionUpdate
): Character => ({
  ...character,
  is_moving: true,
  has_arrived: false,
  in_room: false,
  // @ts-ignore
  way_points: getWayPoints( character, target ),
  needs_elevator: needsElevator( character, target ),
  position: {
    ...character.position,
    id: ''
  },
  target: {
    ...character.target,
    ...target,
    id: target.id || ''
  }
});
