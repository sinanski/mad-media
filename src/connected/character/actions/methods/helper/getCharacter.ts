import {State} from "../../types";

export const getCharacter = (state: State, characterIndex: number) =>
  state[characterIndex];