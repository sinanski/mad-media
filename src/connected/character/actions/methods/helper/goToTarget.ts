import { PositionUpdate } from "../../types";

export const goToTarget = (target: PositionUpdate) => ({
  floor: target.floor,
  x: target.x,
  id: target.id
});
