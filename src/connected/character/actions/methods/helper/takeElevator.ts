import { Character, PositionUpdate } from "../../types";
import { elevator_position } from "../../../../../views/tower/constants/elevatorPosition";

export const takeElevator = (
  character: Character,
  target: PositionUpdate
) => ({
  floor: target.floor,
  is_moving: false,
  x: elevator_position,
  id: 'ride_elevator',
  call_elevator: [ character.position.floor, target.floor ]
});
