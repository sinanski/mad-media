export const mockCharacter = {
  position: {
    floor: 'current floor',
    x: 'current X'
  },
  target: {
    floor: 'target floor',
    x: 'target X'
  }
};