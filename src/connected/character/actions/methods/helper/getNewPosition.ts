import { Store } from "use-global-hook";
import { Actions, Character, PositionUpdate, State, WayPoints } from "../../types";
import { getCharacter } from "./getCharacter";
import { sliceWayPoints } from "./sliceWayPoints";

export const getNewPosition = (
  store: Store<State, Actions>,
  characterIndex: number,
  position: PositionUpdate
) => {
  const character = getCharacter(store.state, characterIndex);
  const way_points = sliceWayPoints( character.way_points );
  const has_arrived = hasArrived(way_points);
  const newPosition = has_arrived ? getArrivedPosition : getMovingPosition;
  const in_room = Boolean(has_arrived && character.target.id);

  return {
    ...character,
    is_moving: false,
    way_points,
    has_arrived,
    in_room,
    position: newPosition(character, position)
  };
};

const hasArrived = (way_points: WayPoints) => {
  return !way_points.length
};

const getArrivedPosition = (
  character: Character,
) => ({
  ...character.target,
  floor: character.target.floor,
  x: character.target.x,
  id: character.target.id,
});

const getMovingPosition = (
  character: Character,
  position: PositionUpdate
) => ({
  ...character.position,
  ...position,
  floor: position.floor || character.position.floor,
  x: position.x || character.position.x,
});
