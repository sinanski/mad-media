export const getId = (index: number): string => {
  const ids = ['', 'mad', 'sun', 'fun'];
  if ( index < 1 || index > 3 ) {

    return ''
  }

  return ids[index];
};
