import { WayPoints } from "../../types";

export const sliceWayPoints = (way_points: WayPoints) =>
  way_points
    .slice()
    .splice( 1, way_points.length - 1 );
