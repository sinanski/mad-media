import { Character, PositionUpdate } from "../../types";
import { needsElevator } from "./needsElevator";
import { goToElevator } from "./goToElevator";
import { takeElevator } from "./takeElevator";
import { goToTarget } from "./goToTarget";

export const getWayPoints = (
  character: Character,
  target: PositionUpdate
): Array<PositionUpdate> => {
  const needs_elevator = needsElevator( character, target );
  if ( needs_elevator ) {
    return [
      goToElevator(),
      takeElevator(character, target),
      goToTarget(target),
    ]
  }
  return [ target ];
};
