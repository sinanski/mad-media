import { Character, PositionUpdate } from "../../types";

export const needsElevator = (
  character: Character,
  target: PositionUpdate
) =>
  character.position.floor !== target.floor;
