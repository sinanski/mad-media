import {getNewPosition} from "../getNewPosition";
import {getCharacter} from "../getCharacter";
import {sliceWayPoints} from "../sliceWayPoints";

jest.mock('../getCharacter', () => ({
  getCharacter: jest.fn(() => ({
    position: {
      floor: 'character floor',
      x: 'character x',
    },
    way_points: 'way_points'
  }))
}));

jest.mock('../sliceWayPoints', () => ({
  sliceWayPoints: jest.fn(() => 'sliced way_points')
}));

const store = {
  state: 'current state',
  way_points: 'way_points',
};

const characterIndex = 'index';

const position = {
  floor: 'position floor',
  x: 'position x',
};


describe('getNewPosition()', () => {
  it('should call getCharacter', () => {
    getNewPosition(store, characterIndex, position);

    expect(getCharacter).toHaveBeenCalledWith(store.state, characterIndex)
  });

  it('should call sliceWayPoints', () => {
    getNewPosition(store, characterIndex, position);

    expect(sliceWayPoints).toHaveBeenCalledWith(store.way_points)
  });

  it('should return merged object', () => {
    const result = getNewPosition(store, characterIndex, position);
    const expected = {
      position: { floor: 'position floor', x: 'position x' },
      way_points: 'sliced way_points',
      is_moving: false
    };

    expect(result).toMatchObject(expected)
  });

  it('should return current Position if no new position is provided', () => {
    const result = getNewPosition(store, characterIndex, {});
    const expected = {
      position: { floor: 'character floor', x: 'character x' },
      way_points: 'sliced way_points',
      is_moving: false
    };

    expect(result).toMatchObject(expected)
  });
});
