import { takeElevator } from "../takeElevator";
import { mockCharacter } from "../mockCharacter";
import { elevator_position } from "../../../../../../views/tower/constants/elevatorPosition";

const mockTarget = {
  floor: 'target floor'
};

describe( 'takeElevator()', () => {
  it( 'should return right object', () => {
    const updatedCharacter = takeElevator( mockCharacter, mockTarget )
    const expected = {
      floor: 'target floor',
      is_moving: false,
      x: elevator_position,
      id: 'ride_elevator',
      call_elevator: [ 'current floor', 'target floor' ]
    };

    expect( updatedCharacter ).toMatchObject( expected )
  } );
} );