import {goToTarget} from "../goToTarget";

const mockTarget = {
  floor: 'target floor',
  x: 'target x',
  id: 'target id',
};

describe('takeElevator()', () => {
  it( 'should return right object', () => {
    const updatedCharacter = goToTarget(mockTarget);

    expect(updatedCharacter).toMatchObject(mockTarget)
  } );
});