import {getWayPoints} from "../getWayPoints";
import { needsElevator } from "../needsElevator";
import { goToElevator } from "../goToElevator";
import { takeElevator } from "../takeElevator";
import { goToTarget } from "../goToTarget";
import {mockCharacter} from "../mockCharacter";

jest.mock('../needsElevator');
jest.mock('../goToElevator');
jest.mock('../takeElevator');
jest.mock('../goToTarget');

const mockTarget = {
  floor: 'target floor'
};

describe('getWayPoints()', () => {
  it( 'should call needsElevator()', () => {
    getWayPoints(mockCharacter, mockCharacter);

    expect(needsElevator).toHaveBeenCalledWith(mockCharacter, mockCharacter)
  } );

  it( 'should return target as array if elevator is not needed', () => {
    const x = getWayPoints(mockCharacter, mockCharacter);

    expect(x).toHaveLength(1);
    expect(x[0]).toMatchObject(mockCharacter);
  } );

  it( 'should return way_points [to_elevator, elevator_ride, to_target] ', () => {
    needsElevator.mockReturnValue(true);
    const x = getWayPoints(mockCharacter, mockTarget);

    expect(goToElevator).toHaveBeenCalled();
    expect(takeElevator).toHaveBeenCalledWith(mockCharacter, mockTarget);
    expect(goToTarget).toHaveBeenCalledWith(mockTarget);
  } );
});
