import {goToElevator} from "../goToElevator";
import { elevator_position } from "../../../../../../views/tower/constants/elevatorPosition";

describe('goToElevator()', () => {
  it( 'should return elevator position and movement id', () => {
    const result = goToElevator();
    const expected = {
      x: elevator_position,
      id: 'go_to_elevator'
    }

    expect(result).toMatchObject(expected);
  } );
});