import {getId} from "../getId";

describe('getId()', () => {
  it( 'should return right id', function () {

    expect(getId(1)).toBe('mad');
    expect(getId(2)).toBe('sun');
    expect(getId(3)).toBe('fun');
  } );

  it( 'should return empty string if id < 1 || id > 3', function () {

    expect(getId(0)).toBe('');
    expect(getId(4)).toBe('');
  } );
});
