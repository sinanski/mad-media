import {sliceWayPoints} from "../sliceWayPoints";

describe('sliceWayPoints()', () => {
  it( 'should return a new array with the first entry removed',  () => {
    const mock_way_points = [
      'first',
      'second',
      'third'
    ];
    const sliced = sliceWayPoints( mock_way_points );

    expect(mock_way_points).toEqual(mock_way_points);
    expect(sliced).toEqual(['second', 'third']);
  } );
});
