import { elevator_position } from "../../../../../views/tower/constants/elevatorPosition";

export const goToElevator = () => ({
  x: elevator_position,
  id: 'go_to_elevator'
});
