import { Store } from "use-global-hook";
import { Actions, Character, State } from "../types";
import {getCharacter} from "./helper/getCharacter";

export const setX = (
  store: Store<State, Actions>,
  characterIndex: number,
  x: number
) => {
  const character = getCharacter(store.state, characterIndex);
  const newCharacter = updateCharacter(character, x);

  store.setState( {
    ...store.state,
    [characterIndex]: newCharacter
  } );
};

const updateCharacter = (
  character: Character,
  x: number
) => ({
  ...character,
  position: {
    ...character.position,
    x
  }
});
