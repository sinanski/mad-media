import { Store } from "use-global-hook";
import { Actions, State } from "../types";
import {getCharacter} from "./helper/getCharacter";

export const leaveRoom = (
  store: Store<State, Actions>,
) => {
  const character = getCharacter(store.state, 1);

  if ( !character.in_elevator ) {
    store.setState( {
      ...store.state,
      1: {
        ...character,
        in_room: false
      }
    } )
  }
};
