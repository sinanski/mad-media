import { Store } from "use-global-hook";
import { Actions, PositionUpdate, State } from "../types";
import { getNewPosition } from "./helper/getNewPosition";

export const arriveAtFloor = (
  store: Store<State, Actions>,
  characterIndex: number,
  position: PositionUpdate
) => {
  const newCharacter = getNewPosition( store, characterIndex, position );

  store.setState( {
    ...store.state,
    [characterIndex]: {
      ...newCharacter,
      needs_elevator: false,
      is_moving: true,
      in_elevator: false
    }
  } )
};
