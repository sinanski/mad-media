import { Store } from "use-global-hook";
import { Actions, Character, State } from "../types";
import { getCharacter } from "./helper/getCharacter";

export const enterElevator = (
  store: Store<State, Actions>,
  characterIndex: number,
) => {
  const character = getCharacter(store.state, characterIndex);
  const newCharacter = getNewCharacter(character);

  store.setState( {
    ...store.state,
    [characterIndex]: newCharacter
  } );
};

const getNewCharacter = (character: Character) => ({
  ...character,
  in_elevator: true
});
