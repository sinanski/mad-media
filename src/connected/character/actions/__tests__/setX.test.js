import {setX} from "../methods/setX";
import {getCharacter} from "../methods/helper/getCharacter";

jest.mock('../methods/helper/getCharacter', () => ({
  getCharacter: jest.fn(() => ({
    in_elevator: 'in elevator',
    target: {
      floor: 'target floor',
      x: 'target x',
    },
  }))
}));

const store = {
  state: 'state',
  setState: jest.fn()
};

const characterIndex = 'character index';

const x = 'x'

describe('setX', () => {

  it( 'should call setState()', () => {
    setX(store, characterIndex, x);
    const expected = {
      ...store.state,
      "character index": {
        "in_elevator": "in elevator",
        "position": {"x": "x"},
        "target": {"floor": "target floor", "x": "target x"}
      }
    };

    expect(store.setState).toHaveBeenCalledWith(expected)
  } );
});