import {initialize} from "../methods/initialize";
import {getCharacter} from "../methods/helper/getCharacter";
import {getId} from "../methods/helper/getId";

jest.mock('../methods/helper/getCharacter');
jest.mock('../methods/helper/getId');

const store = {
  state: {
    foo: 'bar'
  },
  setState: jest.fn()
};

describe('initialize()', () => {

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it( 'should call getCharacter()', function () {
    initialize(store, 1);

    expect(getCharacter).toHaveBeenCalledWith(store.state, 1)
  } );

  it( 'should call getCharacter()', function () {
    initialize(store, 1);

    expect(getId).toHaveBeenCalledWith(1)
  } );

  it( 'should call getCharacter()', function () {
    getId.mockReturnValue('mock id');
    initialize(store, 1);

    const expected = {
      ...store.state,
      1: {
        ...store.state[1],
        id: 'mock id',
        index: 1
      }
    };

    expect(store.setState).toHaveBeenCalledWith(expected)
  } );
});
