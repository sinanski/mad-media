import {arrive} from "../methods/arrive";
import { getNewPosition } from "../methods/helper/getNewPosition";

jest.mock('../methods/helper/getNewPosition');

const store = {
  state: 'state',
  setState: jest.fn()
};

const characterIndex = 'character index';

const position = 'position';

describe('arrive()', () => {
  it( 'should call getNewPosition()', () => {
    arrive(store, characterIndex, position);

    expect(getNewPosition).toHaveBeenCalledWith(store, characterIndex, position)
  } );

  it( 'should call setState()', () => {
    getNewPosition.mockReturnValue('new character')
    arrive(store, characterIndex, position);
    const expected = {
      ...store.state,
      "character index": 'new character'
    };

    expect(store.setState).toHaveBeenCalledWith(expected)
  } );
});