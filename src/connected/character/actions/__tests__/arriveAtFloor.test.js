import {arriveAtFloor} from "../methods/arriveAtFloor";
import { getNewPosition } from "../methods/helper/getNewPosition";

jest.mock('../methods/helper/getNewPosition');

const store = {
  state: 'state',
  setState: jest.fn()
};

const characterIndex = 'character index';

const position = 'position'

describe('arriveAtFloor', () => {
  it( 'should call getNewPosition()', () => {
    arriveAtFloor(store, characterIndex, position);

    expect(getNewPosition).toHaveBeenCalledWith(store, characterIndex, position)
  } );

  it( 'should call setState()', () => {
    arriveAtFloor(store, characterIndex, position);
    const expected = {
      ...store.state,
      "character index": {
        "in_elevator": false,
        "is_moving": true,
        "needs_elevator": false
      }
    }

    expect(store.setState).toHaveBeenCalledWith(expected)
  } );
});