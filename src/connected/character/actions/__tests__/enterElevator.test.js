import { enterElevator } from "../methods/enterElevator";

jest.mock( '../methods/helper/getCharacter', () => ({
  getCharacter: jest.fn( () => ({
    in_elevator: true,
    target: {
      floor: 'target floor',
      x: 'target x',
    },
  }) )
}) );

const store = {
  state: 'state',
  setState: jest.fn()
};

const characterIndex = 'character index';

describe( 'enterElevator()', () => {

  it( 'should call setState with right params', () => {

    enterElevator( store, characterIndex );
    const extepcted = {
      ...store.state,
      "character index": {
        "in_elevator": true,
        "target": { "floor": "target floor", "x": "target x" }
      }
    };

    expect( store.setState ).toHaveBeenCalledWith( extepcted )
  } );
} );
