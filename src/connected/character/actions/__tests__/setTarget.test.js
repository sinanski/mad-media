import {setTarget} from "../methods/setTarget";
import {getCharacter} from "../methods/helper/getCharacter";
import {getWayPoints} from "../methods/helper/getWayPoints";
import {needsElevator} from "../methods/helper/needsElevator";

jest.mock('../methods/helper/getCharacter', () => ({
  getCharacter: jest.fn(() => ({
    in_elevator: true,
    target: {
      floor: 'target floor',
      x: 'target x',
    },
  }))
}));
jest.mock('../methods/helper/getWayPoints');
jest.mock('../methods/helper/needsElevator');

const store = {
  state: 'state',
  setState: jest.fn()
};

const characterIndex = 'character index';

const target = 'target'

describe('setTarget()', () => {
  it( 'setTarget() should call the right methods', () => {
    setTarget(store, characterIndex, target);
    const expectedCharacter = {
      in_elevator: true,
      target: {
        floor: 'target floor',
        x: 'target x',
      },
    };

    expect(getCharacter).toHaveBeenCalledWith(store.state, characterIndex);
    expect(getWayPoints).toHaveBeenCalledWith(expectedCharacter, target);
    expect(needsElevator).toHaveBeenCalledWith(expectedCharacter, target);
  } );

  it( 'should not call setState when character is in elevator',  () => {
    setTarget(store, characterIndex, target);

    expect(store.setState).not.toHaveBeenCalled()
  } );

  it( 'should call setState when character is not in elevator',  () => {
    getCharacter.mockReturnValue({
      in_elevator: false,
      target
    });
    setTarget(store, characterIndex, target);
    const extepcted = {
      ...store.state,
      "character index": {
        "in_elevator": false,
        "has_arrived": false,
        "is_moving": true,
        in_room: false,
        "needs_elevator": undefined,
        "position": {
           "id": "",
        },
        "target": {
          ...target,
          "id": ""
        },
        "way_points": undefined,
      }
    };

    expect(store.setState).toHaveBeenCalledWith(extepcted)
  } );
});
