import React, { useEffect, useRef, useState } from 'react';
import { gsap } from 'gsap';
import Sprite from "./Sprite";
import { Character, Actions, NewPosition } from "./actions/types";
import { Check } from "./check/Check";
import { AnimationProps } from "./props/AnimationProps";
import { State as Elevator } from "../elevator/actions/types";
import { ElevatorActions, State } from "../elevator/actions/types";
import {config} from "../../config";

type MergedCharacter = Character & Actions;
type MergedElevator = State & ElevatorActions;

interface Props {
  characterIndex: number;
  charRef: any;
  character: MergedCharacter;
  prevCharacter?: Character;
  elevator: MergedElevator;
  prevElevator?: Elevator;
  newPosition: NewPosition;
  enterElevator: () => void;
  leaveElevator: () => void;
  callElevator: () => void;
  updateTarget: () => void;
  enterRoom: () => void;
  leaveRoom: () => void;
}

const CharacterAnimator: React.FC<Props> = (
  {
    characterIndex = 1,
    charRef,
    character,
    prevCharacter,
    elevator,
    newPosition,
    enterElevator,
    leaveElevator,
    callElevator,
    updateTarget,
    enterRoom,
    leaveRoom,
    prevElevator,
  }
) => {
  // @ts-ignore
  const timeline = useRef( new gsap.timeline( { paused: true } ) );
  const [ hide, setHide ] = useState(false);

  useEffect( () => {
    const should = new Check( character, elevator, characterIndex, prevCharacter, prevElevator );
    const animationProps = new AnimationProps( charRef, character );

    const walk = () => {
      const duration = animationProps.duration();
      return timeline.current
        .to( charRef.current, {
          duration,
          ease: 'none',
          x: character.way_points[0].x - 40,
          onComplete: () => {
            character.arrive( characterIndex, character.way_points[0] );
          }
        } )
    };

    const move = () => {
      timeline.current.clear();
      walk().play();
    };

    if ( should.move() ) {
      timeline.current.clear();
      updateTarget();
      move();
    }

    if ( should.callElevator() ) {
      callElevator();
    }

    if ( should.enterElevator() ) {
      enterElevator();
    }

    if ( should.leaveElevator() ) {
      leaveElevator();
    }

    if ( should.enterRoom() ) {
      enterRoom();
    }

    if ( should.leaveRoom() ) {
      leaveRoom();
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    elevator,
    character,
  ] );

  // --Delay visibility if character enters room--
  useEffect(() => {
    const timer: ReturnType<typeof setTimeout> = setTimeout(() => {
      const isHidden = character.in_room;
      const wasHidden = prevCharacter && prevCharacter.in_room;

      if ( isHidden && !wasHidden ) {
        setHide(true);
        return () => clearTimeout( timer );
      }

      if ( !isHidden && wasHidden ) {
        setHide(false);
        return () => clearTimeout( timer );
      }
    }, config.character_enter_room_delay || 100)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [character.in_room]);

  // --Do not delay visibility if character enters elevator--
  useEffect(() => {
      const isHidden = character.in_elevator;
      const wasHidden = prevCharacter && prevCharacter.in_elevator;

      if ( isHidden && !wasHidden ) {
        setHide(true);
      }

      if ( !isHidden && wasHidden ) {
        setHide(false);
      }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [character.in_elevator]);


  return (
    <Sprite
      charRef={ charRef }
      character={ character }
      position={ newPosition }
      hide={ hide }
      characterIndex={ characterIndex }
    />
  );
};

export default CharacterAnimator
