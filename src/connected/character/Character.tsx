import React, { Fragment } from 'react';
import CharacterConnector from "./CharacterConnector";
import KI from "../../KI/KI";

interface Props {
  characterIndex: number;
  isNPC?: Boolean;
}

const getId = (index: number) => {
  let id;
  switch ( index ) {
    case (1):
      id = 'mad';
      break;
    case (2):
      id = 'sun';
      break;
    case (3):
      id = 'fun';
      break;
    default :
      console.error( index, 'characterIndex is out of range 1-3' )
  }

  return id;
};

const Character: React.FC<Props> = (
  {
    characterIndex,
    isNPC
  }
) => {
  const id = getId(characterIndex);

  const ki = () =>
    isNPC && id &&
      <KI index={ characterIndex } id={id} />;

  return (
    <Fragment>
      { ki() }
      <CharacterConnector characterIndex={ characterIndex } />
    </Fragment>
  );
};

export default Character
