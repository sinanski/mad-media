import React, { useEffect, useRef } from 'react';
import { usePrevious } from "../../assets/functions/use/usePrevious";
import { useElevator } from "../elevator/store";
import { useCharacter } from "./store";
import { Character, NewPosition } from "./actions/types";
import { State as Elevator } from "../elevator/actions/types";
import CharacterAnimator from "./CharacterAnimator";
import { AnimationProps } from "./props/AnimationProps";
import useRooms from "../rooms/store";

interface Props {
  characterIndex: number;
}

function getNewPosition(character: Character): NewPosition {
  const { way_points, target, position, ...restCharacter } = character;
  return {
    ...restCharacter,
    ...position
  };
}

const CharacterConnnector: React.FC<Props> = ({ characterIndex = 1 }) => {
  const [ char, setCharacter ] = useCharacter();
  const [ elevator, elevatorAction ] = useElevator();
  const [ , setRoom ] = useRooms();
  const charRef = useRef( null );
  const character = char[characterIndex];
  const prevCharacter = usePrevious<Character>( character );
  const prevElevator = usePrevious<Elevator>( elevator );
  const newPosition = getNewPosition( character );

  useEffect( () => {
    setCharacter.initialize( characterIndex )
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [] );

  const enterElevator = () => {
    setCharacter.enterElevator( characterIndex );
    elevatorAction.enter( characterIndex );
  };

  const leaveElevator = () => {
    const next = character.way_points[0];
    elevatorAction.eject();
    setCharacter.arriveAtFloor( characterIndex, next );
  };

  const callElevator = () => {
    const call_to = character.position.floor;
    elevatorAction.call( call_to, characterIndex )
  };

  const updateTarget = () => {
    const x = new AnimationProps( charRef, character )
      .position()
      .x;
    setCharacter.setX( characterIndex, x );
  };

  const enterRoom = () => {
    // @ts-ignore
    setRoom.enter( character.target.id )
  };

  const leaveRoom = () => {
    // @ts-ignore
    setRoom.leave(prevCharacter!.position.id)
  };

  return (
    <CharacterAnimator
      characterIndex={ characterIndex }
      charRef={ charRef }
      character={ { ...character, ...setCharacter } }
      prevCharacter={ prevCharacter }
      elevator={ { ...elevator, ...elevatorAction } }
      prevElevator={ prevElevator }
      newPosition={ newPosition }
      enterElevator={ enterElevator }
      leaveElevator={ leaveElevator }
      callElevator={ callElevator }
      updateTarget={ updateTarget }
      enterRoom={ enterRoom }
      leaveRoom={ leaveRoom }
    />
  );
};

export default CharacterConnnector
