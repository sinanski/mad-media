export const getX = (currentPosition: string): number => {
  const amount = currentPosition
    .split( '(' )[1]
    .split( 'px' )[0];

  return Number( amount )
};
