export const makePositive = (number: number) =>
  number >= 0 ? number : number * -1;
