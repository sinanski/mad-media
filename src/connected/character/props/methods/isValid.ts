export const isValid = (currentPosition?: string): boolean =>
  typeof currentPosition === 'string'
  && currentPosition.includes( '(' )
  && currentPosition.includes( 'px' );
