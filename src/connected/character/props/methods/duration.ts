import { Character } from "../../actions/types";
import { getCurrentPosition } from "./getCurrentPosition";
import { makePositive } from "./makePositive";
import { config } from "../../../../config/index";

type Ref = any;

export const duration = (
  charRef: Ref,
  character: Character,
): number => {
  const currentPosition = getCurrentPosition( charRef, character.position );
  const target = character.way_points[0];
  const dur = (target.x - currentPosition.x - 40) / 100;
  const time = dur / (config.character_speed || 1);
  return makePositive( time ) || .1;
};
