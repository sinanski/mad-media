import {getX} from "../getX";

describe('getX()', () => {
  it('should return x from translate(90px, 0px)', () => {
    const validString = 'translate(90px, 0px)';
    const valid = getX(validString);

    expect(valid).toBe(90);
  })
});
