import {makePositive} from "../makePositive";

describe('makePositive()', () => {
  it( 'should return a positive Value', function () {
    const x = makePositive(1);
    const y = makePositive(-1);

    expect(x).toBe(1);
    expect(y).toBe(1);
  } );
});
