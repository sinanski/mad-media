import { getCurrentPosition } from "../getCurrentPosition";
import { isValid } from "../isValid";
import { getPosition } from "../getPosition";

jest.mock('../isValid');
jest.mock('../getPosition');

// const ref = charRef.current.style.transform
const current_position = 'translate(90px, 0px)';

const ref = {
  current: {
    style: {
      transform: current_position
    }
  }
};

const position = {
  floor: 'current floor',
  x: 'current x'
};



describe('getCurrentPosition', () => {

  beforeEach(() => {
    jest.clearAllMocks()
  });

  it( 'should call isValid()', () => {
    getCurrentPosition(ref, position);

    expect(isValid).toHaveBeenCalledTimes(1);
    expect(isValid).toHaveBeenCalledWith(current_position)
  } );

  it( 'should call getPosition() if isValid()', () => {
    isValid.mockReturnValue(true);
    getCurrentPosition(ref, position);

    expect(getPosition).toHaveBeenCalledTimes(1);
    expect(getPosition).toHaveBeenCalledWith(position, current_position);
  } );

  it( 'should not call getPosition() if !isValid()', () => {
    isValid.mockReturnValue(false);
    getCurrentPosition(ref, position);

    expect(getPosition).not.toHaveBeenCalled()
  } );

  it( 'should return position if !isValid()', () => {
    isValid.mockReturnValue(false);
    const newPosition = getCurrentPosition(ref, position);

    expect(newPosition).toMatchObject(position)
  } );

  it( 'should return the value of getPosition() if isValid()', () => {
    const expected = 'return of getPosition()';
    isValid.mockReturnValue(true);
    getPosition.mockReturnValue(expected);
    const newPosition = getCurrentPosition(ref, position);

    expect(newPosition).toBe(expected);
  } );
});
