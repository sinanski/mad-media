import { isValid } from "../isValid";

describe( 'isValid()', () => {
  it( 'isValid should only be true for (__px, ...)', () => {
    const validString = 'translate(90px, 0px)';
    const invalidString = 'translate()';
    const valid = isValid( validString );
    const invalid = isValid( invalidString );

    expect( valid ).toBeTruthy();
    expect( invalid ).toBeFalsy();
  } );
});
