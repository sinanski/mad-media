import { duration } from "../duration";
import { getCurrentPosition } from "../getCurrentPosition";
import { makePositive } from "../makePositive";
import {config} from "../../../../../config";

jest.mock( '../getCurrentPosition' );
jest.mock( '../makePositive' );

const current_position = 'translate(90px, 0px)';

const ref = {
  current: {
    style: {
      transform: current_position
    }
  }
};

const target = {x: 1000};

const character = {
  way_points: [ target ],
  position: 'position'
}

describe( 'duration()', () => {

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it( 'should call getCurrentPosition', () => {
    getCurrentPosition.mockReturnValue( 2 );
    duration( ref, character );

    expect( getCurrentPosition ).toHaveBeenCalled();
  } );

  it( 'should call makePositive()', () => {
    makePositive.mockReturnValue( 2 );
    duration( ref, character );

    expect( makePositive ).toHaveBeenCalled();
  } );

  it( 'should return (target.x - currentPosition.x - tile_width / 2) / 100', () => {
    const position = {x:500};
    const expectedDuration = (target.x - position.x - 40) / 100;
    const expected = expectedDuration / config.character_speed;
    getCurrentPosition.mockReturnValue( {x:500} );
    duration( ref, character );

    expect( makePositive ).toHaveBeenCalledWith(expected);
  } );
} );