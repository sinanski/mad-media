import {getPosition} from "../getPosition";

jest.mock('../getX', () => ({
  getX: () => 'new x value'
}));

describe('getPosition()', () => {
  it('getPosition should return updated Position', () => {
    const newPosition = getPosition({floor: 0, x: ''});

    expect(newPosition).toMatchObject({ floor: 0, x: 'new x value' })
  });
});
