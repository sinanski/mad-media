import { Position } from "../../actions/types";
import { getX } from "./getX";

export const getPosition = (position: Position, currentPosition: string): Position => {
  const x = getX(currentPosition);
  return {
    ...position,
    x,
  }
};
