import { Position } from "../../actions/types";
import { isValid } from "./isValid";
import { getPosition } from "./getPosition";

type Ref = any;

export const getCurrentPosition = (
  charRef: Ref,
  position: Position,
): Position => {
  const currentPosition = charRef.current.style.transform;
  if ( isValid( currentPosition ) ) {
    return getPosition( position, currentPosition )
  }

  return position
};
