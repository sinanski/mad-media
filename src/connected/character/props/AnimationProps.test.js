import { AnimationProps } from './AnimationProps';
import { getCurrentPosition } from "./methods/getCurrentPosition";
import { duration } from "./methods/duration";

jest.mock('./methods/getCurrentPosition');
jest.mock('./methods/duration');

const charRef = {

};

const position = {
  floor: 'current Floor',
  x: 'current X',
};

const character = {
  position: position
};

describe( 'AnimationProps', () => {
  it( '.position() should call getPosition', () => {
    const current = new AnimationProps(charRef, character);
    getCurrentPosition.mockReturnValue('mock return position');
    const newPosition = current.position();

    expect(getCurrentPosition).toHaveBeenCalledWith(charRef, position)
    expect(newPosition).toBe('mock return position')
  } );

  it( '.position() should call getPosition', () => {
    const current = new AnimationProps(charRef, character);
    duration.mockReturnValue('mock return duration');
    const newDuration = current.duration();
    current.duration();

    expect(duration).toHaveBeenCalledWith(charRef, character);
    expect(newDuration).toBe('mock return duration')
  } );

} );