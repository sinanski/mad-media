import { Character } from "../actions/types";
import {getCurrentPosition} from "./methods/getCurrentPosition";
import {duration} from "./methods/duration";

// const tileWidth = 80;
// const elevatorTile = 4;

type Ref = any;

export class AnimationProps {
  constructor (
    private charRef: Ref,
    private character: Character
  ) {

  }
  position () {
    return getCurrentPosition(this.charRef, this.character.position)
  }

  duration () {
    return duration(this.charRef, this.character)
  }
}
