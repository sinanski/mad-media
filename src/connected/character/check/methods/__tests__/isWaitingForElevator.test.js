import { isWaitingForElevator } from "../isWaitingForElevator";

const mockCharacter = {
  needs_elevator: true,
  position: {
    x: 365,
    floor: 'mock floor'
  }
};

const mockElevator = {
  current_floor: 'mock floor'
};

describe( 'isWaitingForElevator', () => {
  it( 'should return false if position.x !== Elevator Position', function () {
    const character = {
      ...mockCharacter,
      position: {
        ...mockCharacter.position,
        x: 300
      }
    };
    const result = isWaitingForElevator( character )

    expect( result ).toBeFalsy();
  } );

  it( 'should return false if character.floor !== elevator.floor', function () {
    const character = {
      ...mockCharacter,
      position: {
        ...mockCharacter.position,
        floor: 'different floor'
      }
    }
    const result = isWaitingForElevator( character, mockElevator )

    expect( result ).toBeFalsy();
  } );

  it( 'should return false if !character.needs_elevator', function () {
    const character = {
      ...mockCharacter,
      needs_elevator: false
    };
    const result = isWaitingForElevator( character, mockElevator )

    expect( result ).toBeFalsy();
  } );

  it( 'should return true if all conditions match', function () {
    const result = isWaitingForElevator( mockCharacter, mockElevator )

    expect( result ).toBeTruthy();
  } );
} );
