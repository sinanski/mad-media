import { enterElevator } from "../enterElevator";
import { isWaitingForElevator } from "../isWaitingForElevator";

jest.mock( '../isWaitingForElevator' );

const mockElevator = {
  is_open: true,
  is_empty: true,
  called_by: ['first character', 'second character']
};

const mockCharacter = {
  index: 'first character'
};

const mockSecondCharacter = {
  index: 'second character'
};

describe( 'enterElevator()', () => {
  it( 'should call isWaitingForElevator()', function () {
    const result = enterElevator('character', mockElevator)

    expect(isWaitingForElevator).toHaveBeenCalledWith('character', mockElevator);
    expect(result).toBeFalsy();
  } );

  it( 'should return false id !is_open', function () {
    isWaitingForElevator.mockReturnValue(true);
    const elevator = {
      is_open: false
    };
    const result = enterElevator('character', elevator)

    expect(result).toBeFalsy();
  } );

  it( 'should return false id !is_empty', function () {
    isWaitingForElevator.mockReturnValue(true);
    const elevator = {
      ...mockElevator,
      is_empty: false
    };
    const result = enterElevator('character', elevator)

    expect(result).toBeFalsy();
  } );

  it( 'should return false if character is not the next in line', function () {
    isWaitingForElevator.mockReturnValue(true);
    const result = enterElevator(mockSecondCharacter, mockElevator);

    expect(result).toBeFalsy();
  } );

  it( 'should return true if everything matches', function () {
    isWaitingForElevator.mockReturnValue(true);
    const result = enterElevator(mockCharacter, mockElevator);

    expect(result).toBeTruthy();
  } );
} );
