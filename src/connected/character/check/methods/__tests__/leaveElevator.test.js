import { leaveElevator } from "../leaveElevator";
import { arrivedAtTargetFloor } from "../helper/arrivedAtTargetFloor";
import { characterIsInElevator } from "../helper/characterIsInElevator";

jest.mock('../helper/arrivedAtTargetFloor');
jest.mock('../helper/characterIsInElevator');

const mockCharacter = {

};

const mockOpen = {
  is_open: true
};

const mockClosed = {
  is_open: false
};

const mockWasNotOpen = {
  is_open: false
};

describe( 'leaveElevator()', () => {
  it( 'should call arrivedAtTargetFloor but not characterIsInElevator when !arrived', function () {
    const result = leaveElevator(mockCharacter, mockOpen, 'index');

    expect(arrivedAtTargetFloor).toHaveBeenCalledWith(mockCharacter, mockOpen);
    expect(characterIsInElevator).not.toHaveBeenCalled();
    expect(result).toBeFalsy();
  } );

  it( 'should call characterIsInElevator when arrived', function () {
    arrivedAtTargetFloor.mockReturnValue(true);
    const result = leaveElevator(mockCharacter, mockOpen, 'index');

    expect(arrivedAtTargetFloor).toHaveBeenCalledWith(mockCharacter, mockOpen);
    expect(characterIsInElevator).toHaveBeenCalledWith(mockOpen, 'index');
    expect(result).toBeFalsy();
  } );

  it( 'should return false if !isOpen', function () {
    arrivedAtTargetFloor.mockReturnValue(true);
    characterIsInElevator.mockReturnValue(true);

    const result = leaveElevator(null, mockClosed);

    expect(result).toBeFalsy();
  } );

  it( 'should return true false arrivedAtTargetFloor && characterIsInElevator && isOpen && wasOpen', function () {
    arrivedAtTargetFloor.mockReturnValue(true);
    characterIsInElevator.mockReturnValue(true);

    const result = leaveElevator(null, mockOpen);

    expect(result).toBeFalsy();
  } );

  it( 'should return true if arrivedAtTargetFloor && characterIsInElevator && isOpen && !wasOpen', function () {
    arrivedAtTargetFloor.mockReturnValue(true);
    characterIsInElevator.mockReturnValue(true);

    const result = leaveElevator(null, mockOpen, 0, mockWasNotOpen);

    expect(result).toBeTruthy();
  } );
} );
