import {enterRoom} from "../enterRoom";
import {arrive} from "../helper/arrive";

jest.mock("../helper/arrive");

const mockId = {
  position: {
    id: 'mock id'
  }
};

const missingId = {
  position: {
    id: undefined
  }
};

describe('enterRoom', () => {
  it( 'should return true if a target is given and no prevCharacter is available', () => {
    arrive.mockReturnValue(true)
    const result = enterRoom(mockId)

    expect(result).toBeTruthy();
  } );

  it( 'should return false if target is same as prevTarget', () => {
    const result = enterRoom(missingId)

    expect(result).toBeFalsy();
  } );
});
