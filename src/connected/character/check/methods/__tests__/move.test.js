import { move } from "../move";
import { isChange } from "../helper/isChangeToMove";

jest.mock('../helper/isChangeToMove');

const isMoving = {
  is_moving: true
};

const isNotMoving = {
  is_moving: false
};

describe('move', () => {

  beforeEach(() => {
    jest.clearAllMocks()
  });

  it( 'should return true if isMoving and isChange', () => {
    isChange.mockReturnValue(true);
    const result = move(isMoving);

    expect(result).toBeTruthy();
  } );

  it( 'should return false if !isMoving', () => {
    isChange.mockReturnValue(true);
    const result = move(isNotMoving);

    expect(result).toBeFalsy();
  } );

  it( 'should return false if !isChange', () => {
    isChange.mockReturnValue(false);
    const result = move(isMoving);

    expect(result).toBeFalsy();
  } );
});
