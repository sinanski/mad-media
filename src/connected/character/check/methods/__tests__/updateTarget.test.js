import {updateTarget} from "../updateTarget";

const character = {
  target: 'current Target'
};

describe('updateTarget', () => {
  it( 'should return true if a target is given and no prevCharacter is available', () => {
    const result = updateTarget(character)

    expect(result).toBeTruthy();
  } );

  it( 'should return false if target is same as prevTarget', () => {
    const result = updateTarget(character, character)

    expect(result).toBeFalsy();
  } );
});
