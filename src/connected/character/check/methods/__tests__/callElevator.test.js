import { callElevator } from "../callElevator";
import { stop } from "../helper/isChangeToMove";

jest.mock('../helper/isChangeToMove');

const needsElevator = {
  needs_elevator: true
};

const needsNoElevator = {
  needs_elevator: false
};

describe('callElevator()', () => {
  it( 'should return true if stop() is true and needsElevator', function () {
    stop.mockReturnValue(true);
    const result = callElevator(needsElevator);

    expect(result).toBeTruthy();
  } );

  it( 'should return false if !stop()', function () {
    stop.mockReturnValue(true);
    const result = callElevator(needsNoElevator);

    expect(result).toBeFalsy();
  } );

  it( 'should return false if !needsElevator', function () {
    stop.mockReturnValue(false);
    const result = callElevator(needsElevator);

    expect(result).toBeFalsy();
  } );
});
