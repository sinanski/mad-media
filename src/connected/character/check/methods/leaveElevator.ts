import { Character } from "../../actions/types";
import { State as ElevatorState } from "../../../elevator/actions/types";
import { arrivedAtTargetFloor } from "./helper/arrivedAtTargetFloor";
import { characterIsInElevator } from "./helper/characterIsInElevator";
import {State as Elevator} from "../../../elevator/actions/types";

export const leaveElevator = (
  character: Character,
  elevator: ElevatorState,
  characterIndex: number,
  prevElevator?: Elevator,
) =>
  Boolean(
    arrivedAtTargetFloor( character, elevator )
    && characterIsInElevator(elevator, characterIndex)
    && elevator.is_open
    && prevElevator
    && !prevElevator.is_open
  );
