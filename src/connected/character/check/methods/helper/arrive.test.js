import {arrive} from "./arrive";

const character = {
  has_arrived: true
};

const prevCharacter = {
  has_arrived: false
};

describe('arrive', () => {
  it( 'should return true if a target is given and no prevCharacter is available', () => {
    const result = arrive(character, prevCharacter)

    expect(result).toBeTruthy();
  } );

  it( 'should return false if target is same as prevTarget', () => {
    const result = arrive(character, character)

    expect(result).toBeFalsy();
  } );
});
