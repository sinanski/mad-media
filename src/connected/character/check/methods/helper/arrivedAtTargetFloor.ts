import { Character } from "../../../actions/types";
import { State as ElevatorState } from "../../../../elevator/actions/types";

export const arrivedAtTargetFloor = (
  character: Character,
  elevator: ElevatorState,
) =>
  Boolean(
    elevator.has_arrived
    && elevator.current_floor === character.target.floor
  );
