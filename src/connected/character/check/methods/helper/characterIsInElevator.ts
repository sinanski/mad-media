import { State as ElevatorState } from "../../../../elevator/actions/types";

export const characterIsInElevator = (
  elevator: ElevatorState,
  characterIndex: number
) =>
  elevator.character === characterIndex;
