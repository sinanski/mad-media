import { Character } from "../../../actions/types";

export const arrive = (
  character: Character,
  prevCharacter: Character
): Boolean =>
  Boolean(
    character.has_arrived
    && !prevCharacter.has_arrived
  );
