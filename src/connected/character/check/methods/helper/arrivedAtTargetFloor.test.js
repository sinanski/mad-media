import { arrivedAtTargetFloor } from "./arrivedAtTargetFloor";

const mockElevator = {
  has_arrived: true,
  current_floor: 'mock current floor'
};

const mockCharacter = {
  target: {
    floor: 'mock current floor'
  }
};

describe('arrivedAtTargetFloor()', () => {
  it( 'should return true if elevator has arrived && elevatorFloor === character.targetFloor', function () {
    const result = arrivedAtTargetFloor(mockCharacter, mockElevator);

    expect(result).toBeTruthy();
  } );

  it( 'should return false if !elevator has arrived', function () {
    const mockNotArrived = {
      ...mockElevator,
      has_arrived: false
    };
    const result = arrivedAtTargetFloor(mockCharacter, mockNotArrived);

    expect(result).toBeFalsy();
  } );

  it( 'should return false if currentFloor is not targetFloor', function () {
    const mockDifferentFloor = {
      ...mockElevator,
      current_floor: 'different'
    };
    const result = arrivedAtTargetFloor(mockCharacter, mockDifferentFloor);

    expect(result).toBeFalsy();
  } );
});
