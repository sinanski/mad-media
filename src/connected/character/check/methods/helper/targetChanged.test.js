import { targetChanged } from "./targetChanged";

const character = {
  target: 'mock target'
};

const prevCharacter = {
  target: 'mock prev target'
};

describe('targetChanged()', () => {
  it( 'should return true if target !== prevTarget', () => {
    const result = targetChanged(character, prevCharacter);

    expect(result).toBeTruthy();
  } );

  it( 'should return false if target === prevTarget', () => {
    const result = targetChanged(character, character);

    expect(result).toBeFalsy();
  } );
});