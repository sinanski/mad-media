import { Character } from "../../../actions/types";

export const targetChanged = (
  character: Character,
  prevCharacter: Character
) =>
  Boolean(
    character.target !== prevCharacter.target
  );
