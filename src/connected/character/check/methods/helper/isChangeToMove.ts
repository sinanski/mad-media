import { Character } from "../../../actions/types";
import { targetChanged } from "./targetChanged";

export const isChange = (
  character: Character,
  prevCharacter: Character
) =>
  Boolean(
    !prevCharacter.is_moving
    || targetChanged( character, prevCharacter )
  );

export const stop = (
  character: Character,
  prevCharacter: Character
) => Boolean(
  !character.is_moving
  && prevCharacter.is_moving
);
