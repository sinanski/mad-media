import {characterIsInElevator} from "./characterIsInElevator";

const mockElevator = {
  character: 'mock index'
}

describe('characterIsInElevator()', () => {
  it( 'should return true if characterIndex === elevator.character', function () {
    const result = characterIsInElevator(mockElevator, 'mock index');

    expect(result).toBeTruthy();
  } );

  it( 'should return false if characterIndex !== elevator.character', function () {
    const result = characterIsInElevator(mockElevator, 'different index');

    expect(result).toBeFalsy();
  } );
});
