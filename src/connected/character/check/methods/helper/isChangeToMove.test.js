import { isChange, stop } from "./isChangeToMove";
import {targetChanged} from "./targetChanged";

jest.mock('./targetChanged');

const isMoving = {
  is_moving: true
};

const notMoving = {
  is_moving: false
};

describe('isChangeToMove()', () => {

  beforeEach(() => {
    jest.clearAllMocks()
  });

  it( 'should return true if !wasMoving', () => {
    const result = isChange(isMoving, notMoving);

    expect(result).toBeTruthy();
  } );

  it( 'should return true if targetChanged', () => {
    targetChanged.mockReturnValue(true);
    const result = isChange(isMoving, isMoving);

    expect(result).toBeTruthy();
  } );

  it( 'should call targetChanged()', () => {
    isChange(notMoving, isMoving);

    expect(targetChanged).toHaveBeenCalledWith(notMoving, isMoving)
  } );
});

describe('stop()', () => {
  it( 'should return true if wasMoving and !isMoving', function () {
    const x = stop(notMoving, isMoving);

    expect(x).toBeTruthy();
  } );

  it( 'should return false if isMoving', function () {
    const x = stop(isMoving);

    expect(x).toBeFalsy();
  } );

  it( 'should return false if !wasMoving', function () {
    const x = stop(notMoving, notMoving);

    expect(x).toBeFalsy();
  } );
});
