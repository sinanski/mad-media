import { Character } from "../../actions/types";
import {stop} from "./helper/isChangeToMove";

export const callElevator = (
  character: Character,
  prevCharacter: Character
) =>
  Boolean(
    stop(character, prevCharacter)
    && character.needs_elevator
  );
