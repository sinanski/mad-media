import { Character } from "../../actions/types";
import { State as ElevatorState } from "../../../elevator/actions/types";

export const isWaitingForElevator = (
  character: Character,
  elevator: ElevatorState,
) =>
  Boolean(
    character.position.x === 365
    && character.position.floor === elevator.current_floor
    && character.needs_elevator
  );
