import { Character } from "../../actions/types";
import { arrive } from "./helper/arrive";

export const enterRoom = (
  character: Character,
  prevCharacter: Character
) => {
  const has_arrived = arrive( character, prevCharacter );
  return Boolean( has_arrived && character.position.id );
};
