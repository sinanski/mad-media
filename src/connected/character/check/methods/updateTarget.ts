import { Character } from "../../actions/types";

export const updateTarget = (
  character: Character,
  prevCharacter?: Character
) => {
  const prevTarget = prevCharacter && prevCharacter.target;
  return Boolean( character.target !== prevTarget
  )
};
