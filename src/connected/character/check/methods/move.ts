import { Character } from "../../actions/types";
import { isChange } from "./helper/isChangeToMove";

export const move = (
  character: Character,
  prevCharacter: Character
) =>
  Boolean(
    character.is_moving
    && isChange(character, prevCharacter)
  );
