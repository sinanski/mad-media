import { Character } from "../../actions/types";
import { State as ElevatorState } from "../../../elevator/actions/types";
import { isWaitingForElevator } from "./isWaitingForElevator";

export const enterElevator = (
  character: Character,
  elevator: ElevatorState,
) =>
  Boolean(
    isWaitingForElevator(character, elevator)
    && elevator.is_open
    && elevator.is_empty
    && character.index === elevator.called_by[0]
  );
