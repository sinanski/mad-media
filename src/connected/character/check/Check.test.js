import { Check } from "./Check";
import { getPrevCharacter } from "./getPrevCharacter";
import { move } from "./methods/move";
import { callElevator } from "./methods/callElevator";
import { leaveElevator } from "./methods/leaveElevator";
import { enterElevator } from "./methods/enterElevator";
import { enterRoom } from "./methods/enterRoom";

jest.mock( './getPrevCharacter', () => ({
  getPrevCharacter: jest.fn( () => 'mock prev character' )
}) );

jest.mock('./methods/move');
jest.mock('./methods/callElevator');
jest.mock('./methods/leaveElevator');
jest.mock('./methods/enterElevator');
jest.mock('./methods/enterRoom');

const mockCharacter = 'mock character';
const mockElevator = 'mock elevator';
const mockIndex = 'mock index';
const mockPrevCharacter = 'mock prev character';
const mockPrevElevator = 'mock prev elevator';

const should = new Check( mockCharacter, mockElevator, mockIndex, mockPrevCharacter, mockPrevElevator );

describe( 'new Check()', () => {
  it( 'getPrevCharacter should be called', () => {

    expect( getPrevCharacter ).toHaveBeenCalledWith( mockCharacter, mockPrevCharacter )
  } );

  it( '.move() should call move with right arguments', () => {
    should.move();

    expect( move ).toHaveBeenCalledWith( mockCharacter, mockPrevCharacter )
  } );

  it( '.callElevator() should call callElevator move with right arguments', () => {
    should.callElevator();

    expect( callElevator ).toHaveBeenCalledWith( mockCharacter, mockPrevCharacter )
  } );

  it( '.leaveElevator() should call leaveElevator move with right arguments', () => {
    should.leaveElevator();

    expect( leaveElevator ).toHaveBeenCalledWith( mockCharacter, mockElevator, mockIndex, mockPrevElevator )
  } );

  it( '.enterElevator() should call enterElevator move with right arguments', () => {
    should.enterElevator();

    expect( enterElevator ).toHaveBeenCalledWith( mockCharacter, mockElevator )
  } );

  it( '.enterRoom() should call enterRoom move with right arguments', () => {
    should.enterRoom();

    expect( enterRoom ).toHaveBeenCalledWith( mockCharacter, mockPrevCharacter )
  } );
} );
