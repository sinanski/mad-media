import { Character } from "../actions/types";
import { State as ElevatorState } from "../../elevator/actions/types";
import { enterRoom } from "./methods/enterRoom";
import { move } from "./methods/move";
import { callElevator } from "./methods/callElevator";
import { leaveElevator } from "./methods/leaveElevator";
import { enterElevator } from "./methods/enterElevator";
import { getPrevCharacter } from "./getPrevCharacter";
import { State as Elevator } from "../../elevator/actions/types";

export class Check {
  move: () => boolean;
  callElevator: () => boolean;
  leaveElevator: () => boolean;
  enterElevator: () => boolean;
  enterRoom: () => boolean;
  leaveRoom: () => boolean;

  constructor(
    private character: Character,
    private elevator: ElevatorState,
    private index: number,
    private prevCharacter?: Character,
    private prevElevator?: Elevator,
  ) {
    const prevChar = getPrevCharacter( this.character, this.prevCharacter );

    this.move = () => move( this.character, prevChar );
    this.callElevator = () => callElevator( this.character, prevChar );
    this.leaveElevator = () => leaveElevator( this.character, this.elevator, this.index, this.prevElevator );
    this.enterElevator = () => enterElevator( this.character, this.elevator );
    this.enterRoom = () => enterRoom( this.character, prevChar );
    this.leaveRoom = () => leaveRoom( this.character, prevChar );
  }
}

const leaveRoom = (character: Character, prevCharacter?: Character) =>
  Boolean(
    !character.in_room
    && prevCharacter
    && prevCharacter.in_room
  );
