import { Character } from "../actions/types";

export const getPrevCharacter = (
  character: Character,
  prevCharacter?: Character
) => {
  if ( prevCharacter ) {

    return prevCharacter
  }

  return character
};
