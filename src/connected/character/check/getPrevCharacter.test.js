import {getPrevCharacter} from "./getPrevCharacter";

const prevCharacter = 'mock prevCharacter';
const character = 'mock character';

describe('getPrevCharacter()', () => {
  it( 'should return prevCharacter if it exists', function () {
    const result = getPrevCharacter(character, prevCharacter);

    expect(result).toBe(prevCharacter);
  } );

  it( 'should return character if !prevCharacter', function () {
    const result = getPrevCharacter(character);

    expect(result).toBe(character);
  } );
});
