import styled from "styled-components";

interface StyledProps {
  floor: number;
  grid: string;
}

export const StyledTile = styled.div<StyledProps>`
  display: flex;
  pointer-events: none;
  grid-area: ${props => props.grid};
  background: transparent;
`;

export const Image = styled.img<{hasInteraction: boolean}>`
  position:relative;
  cursor: ${ props => props.hasInteraction ? 'pointer' : 'initial' };
  pointer-events: ${ props => props.hasInteraction ? 'initial' : 'none' };
  z-index: ${ props => props.hasInteraction ? 20 : 10 };
`;
