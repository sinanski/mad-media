import React, { useEffect, useRef } from 'react';
import { StyledTile, Image } from "./Styles";
import { RoomType } from "../../../connected/rooms/room_constructor/RoomConstructor";
import { RoomInterface } from "../../../connected/rooms/actions/types";

export interface Props {
  grid_position: string;
  doorProps: RoomType;
  onClick: () => void;
  onClose: () => void;
  state: RoomInterface;
};

const Door: React.FC<Props> = (
  {
    doorProps,
    grid_position,
    onClick,
    onClose,
    state
  }
) => {
  const {
    id,
    image,
    image_open,
    floor
  } = doorProps;

  const { is_open } = state;

  const img = is_open ? image_open : image;

  useEffect(() => {
    const timer = setTimeout( () => {
      onClose();
      return () => clearTimeout( timer );
    }, 200 );
  }, [onClose, is_open]);

  return (
    <StyledTile
      id={ id }
      floor={ floor }
      onClick={ onClick }
      grid={ grid_position }
    >
      { image &&
        <Image src={ img } hasInteraction={ true } />
      }
    </StyledTile>
  );
};

export default Door;
