import Door from "./Door";
import { RoomType } from "../../../connected/rooms/room_constructor/RoomConstructor";
import React, { useCallback, useRef } from "react";
import { useCharacter } from "../../../connected/character/store";
import useRooms from "../../../connected/rooms/store";

export interface Props {
  grid_position: string;
  doorProps: RoomType;
}

const DoorProvider: React.FC<Props> = (
  {
    doorProps,
    grid_position
  }
) => {
  const { id } = doorProps;
  const [ , setCharacter ] = useCharacter();
  const [ rooms, setRooms ] = useRooms();
  const room = useRef( rooms[id] ).current;
  const onClick = () => setCharacter.setTarget( 1, room );
  const onClose = () => setRooms.closeDoor( id );
  const storedOnclick = useCallback( onClick, [] );
  const storedOnClose = useCallback( onClose, [] );

  return (
    <Door
      doorProps={ doorProps }
      state={ rooms[id] }
      grid_position={ grid_position }
      onClick={ storedOnclick }
      onClose={ storedOnClose }
    />
  )
};

export default DoorProvider;
