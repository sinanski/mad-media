import React from 'react';
import {StyledTile, Image} from "./Styles";

interface Props {
  floor: number;
  image?: string;
  style?: {};
  grid_position: string;
}

const Decoration: React.FC<Props> = (
  {
    floor,
    image,
    style,
    grid_position
  }
) => {
  // const correctFloor = floor
  return (
    <StyledTile
      id={ 'decoration' }
      floor={ floor }
      grid={grid_position}
    >
      { image &&
        <Image src={ image } style={ style } hasInteraction={ false } />
      }
    </StyledTile>
  );
};

export default Decoration;
