import React, { Fragment, useRef } from 'react';
import Door from "./DoorProvider";
import Decoration from "./Decoration";
import {
  Floor,
  RoomType,
  DecorationType,
  RoomOrDecoration
} from "../../../connected/rooms/room_constructor/RoomConstructor";


interface Props {
  floor: Floor;
}

const mapTiles = (props: Props) => {
  const {
    floor,
  } = props;

  return floor.map( (tile, i) => {

    if ( !tile ) {
      return null;
    }

    if ( isRoom( tile ) ) {
      return createDoor( tile, i )
    }

    return createDecoration( tile, i );
  } )
};

const createDoor = (door: RoomType, index: number) => (
  <Door
    key={ index }
    grid_position={ getGridPosition( door ) }
    doorProps={ door }
  />
);

const createDecoration = (decoration: DecorationType, index: number) => (
  <Decoration
    key={ index }
    image={ decoration.image }
    floor={ decoration.floor }
    grid_position={ getGridPosition( decoration ) }
  />
);

const getGridPosition = (tile: RoomOrDecoration) =>
  `floor${ tile.floor }_tile${ tile.tile }`;

const TileMapper: React.FC<Props> = (props) => {
  const tiles = useRef( mapTiles( props ) );

  return (
    <Fragment>
      { tiles.current }
    </Fragment>
  );
};

function isRoom(toBeDetermined: RoomOrDecoration): toBeDetermined is RoomType {
  return !!(toBeDetermined as RoomType).id
}

export default TileMapper;
