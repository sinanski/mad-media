import styled from "styled-components";

const template = `
  "floor12_tile0 floor12_tile1 floor12_tile2 floor12_tile3 elevator floor12_tile5 floor12_tile6 floor12_tile7 floor12_tile8"
  "floor11_tile0 floor11_tile1 floor11_tile2 floor11_tile3 elevator floor11_tile5 floor11_tile6 floor11_tile7 floor11_tile8"
  "floor10_tile0 floor10_tile1 floor10_tile2 floor10_tile3 elevator floor10_tile5 floor10_tile6 floor10_tile7 floor10_tile8"
  "floor9_tile0 floor9_tile1 floor9_tile2 floor9_tile3 elevator floor9_tile5 floor9_tile6 floor9_tile7 floor9_tile8"
  "floor8_tile0 floor8_tile1 floor8_tile2 floor8_tile3 elevator floor8_tile5 floor8_tile6 floor8_tile7 floor8_tile8"
  "floor7_tile0 floor7_tile1 floor7_tile2 floor7_tile3 elevator floor7_tile5 floor7_tile6 floor7_tile7 floor7_tile8"
  "floor6_tile0 floor6_tile1 floor6_tile2 floor6_tile3 elevator floor6_tile5 floor6_tile6 floor6_tile7 floor6_tile8"
  "floor5_tile0 floor5_tile1 floor5_tile2 floor5_tile3 elevator floor5_tile5 floor5_tile6 floor5_tile7 floor5_tile8"
  "floor4_tile0 floor4_tile1 floor4_tile2 floor4_tile3 elevator floor4_tile5 floor4_tile6 floor4_tile7 floor4_tile8"
  "floor3_tile0 floor3_tile1 floor3_tile2 floor3_tile3 elevator floor3_tile5 floor3_tile6 floor3_tile7 floor3_tile8"
  "floor2_tile0 floor2_tile1 floor2_tile2 floor2_tile3 elevator floor2_tile5 floor2_tile6 floor2_tile7 floor2_tile8"
  "floor1_tile0 floor1_tile1 floor1_tile2 floor1_tile3 elevator floor1_tile5 floor1_tile6 floor1_tile7 floor1_tile8"
  "floor0_tile0 floor0_tile1 floor0_tile2 floor0_tile3 elevator floor0_tile5 floor0_tile6 floor0_tile7 floor0_tile8"
`;

export const Grid = styled.div`
  position:relative;
  grid-column: 2;
  grid-row: 3;
  display: grid;
  height: 1170px;
  width: 720px;
  grid-template-columns: repeat(9, 80px [col-start]);
  grid-template-rows: repeat(13, 80px [row-start]);
  grid-row-gap: 10px;
  grid-template-areas: ${ template };
  background-color: black;
`;
