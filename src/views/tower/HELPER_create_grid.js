const floors = 13;
const tiles = 8;

export const createGrid = () => {
  let grid_result = '';
  for ( let i = 0; i < floors; i++ ) {
    grid_result += `"${createFloor(i)}"`
  }

  console.log(grid_result)
  return grid_result
};

const createFloor = (index) => {
  let floor_string = '';
  const current_floor = 12 - index;
  for ( let j = 0; j < tiles + 1; j++ ) {
    const current_tile = j < tiles / 2 ? j : j - 1;
    if ( j === tiles / 2 ) {
      floor_string += 'elevator '
    } else {
      floor_string += `floor${current_floor}_tile${current_tile} `
    }
  }

  return floor_string;
};
