import imgUsher from '../../../assets/img/tower/usher.png';
import imgFloorPlan from '../../../assets/img/tower/floor_plan.png';

export interface Room {
  name: string;
  door?: number;
  img?: string;
}

export interface Rooms {
  [index: string]: Room
}

export const rooms: Rooms = {
  betty: {
    name: 'Bettys Büro',
  },
  bertram: {
    name: 'Bertrams Büro',
  },
  real_estate: {
    name: 'Makler H.G. Higwig',
  },
  peace_brothers: {
    name: 'Peace-Brothers - Internationale Friedensliga',
    door: 2
  },
  office_sun: {
    name: 'Büro Sun-TV',
    door: 3,
  },
  news_sun: {
    name: 'Nachrichten-Studio von Sun-TV',
    door: 3,
  },
  boss_sun: {
    name: 'Chef von Sun-TV',
    door: 3,
  },
  archive_sun: {
    name: 'Archiv von Sun-TV',
    door: 3,
  },
  office_fun: {
    name: 'Büro Fun-TV',
    door: 3,
  },
  news_fun: {
    name: 'Nachrichten-Studio von Fun-TV',
    door: 3,
  },
  boss_fun: {
    name: 'Chef von Fun-TV',
    door: 3,
  },
  archive_fun: {
    name: 'Archiv von Fun-TV',
    door: 3,
  },
  office_mad: {
    name: 'Büro Mad-TV',
    door: 3,
  },
  news_mad: {
    name: 'Nachrichten-Studio von Mad-TV',
    door: 3,
  },
  boss_mad: {
    name: 'Chef von Mad-TV',
    door: 3,
  },
  archive_mad: {
    name: 'Archiv von Mad-TV',
    door: 3,
  },
  commercials: {
    name: 'Agentur für Werbung - Inhaber: P.P. Purningham',
  },
  free_studio: {
    name: 'Freies Studio',
    door: 2,
  },
  free_studio_2: {
    name: 'Freies Studio',
    door: 2,
  },
  free_studio_3: {
    name: 'Freies Studio',
    door: 2,
  },
  gun_agency: {
    name: 'Knarren Agentur',
    door: 2,
  },
  consulat_duban: {
    name: 'Konsulat der Volksrepublik Duban',
    door: 2,
  },
  consulat_free_duban: {
    name: 'Konsulat der Freien Republik Duban',
  },
  film_agency: {
    name: 'Film-Agentur - J. Steward Jabble',
    door: 2,
  },
  screenplay: {
    name: 'Drehbuchagentur - John S. Jibble',
  },
  tobacco_abuse: {
    name: 'Amerikanische Gesellschaft gegen Nikotinmissbrauch',
  },
  tobacco_lobby: {
    name: 'Vereinigung amerikanischer Tabakproduzenten',
  },
  psychiatry: {
    name: 'Psychiatriebüro Harry Dippl',
    door: 2,
  },
  bio_control: {
    name: 'Bio-Control - Biodynamischer Monster-Anbau',
    door: 2,
  },
  laundry: {
    name: 'Wäscherei',
    door: 2,
  },
  super_market: {
    name: 'Supermarkt',
  },
  usher: {
    name: 'Pförtner',
    img: imgUsher,
  },
  floor_plan: {
    name: 'Hinweistafel',
    img: imgFloorPlan
  },
};
