import React from 'react';
import styled from 'styled-components';
import { useCharacter } from "../../../connected/character/store";

type InRoom = { isShown: Boolean };

const Container = styled.div<InRoom>`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  pointer-events: none;
  grid-row-start: 1;
  grid-row-end: 2;
  grid-column-start: 1;
  grid-column-end: 2;
  //background: red;
  z-index: 100;
`;

const Content = styled.div`
  display: none;
  position: absolute;
  pointer-events: initial;
  width: 100%;
  height: 100%;
  max-width: 0;
  max-height: 0;
  transition: max-width .8s ease, max-height .8s ease;
`;

const Close = styled.button`
  position: absolute;
  pointer-events: initial;
  top: 20px;
  right: 20px;
  width: 40px;
  height: 40px;
  background-color: black;
  z-index: 200;
`;

const RoomContainer:React.FC<{className?: string}> = ({className}) => {
  const [ char, setChar ] = useCharacter();
  // import [, setRooms]
  const character = char[1];

  const onClose = () => {
    console.log(character)
    setChar.leaveRoom();
  };

  return (
    <Container
      id="Room"
      isShown={ character.in_room }
      className={ className }
    >
      <Close onClick={ onClose } />
      <Content className={ !character.in_room ? '' : 'show' }>
        {character.position.id}
      </Content>
    </Container>
  );
};

const StyledRoom = styled(RoomContainer)`
  .show {
    display: block;
    background-color: blueviolet;
    max-width: 100%;
    max-height: 100%;
  }
`;

export default StyledRoom;


