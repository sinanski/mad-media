import plant from '../../../assets/img/tower/plant.png';
import plant2 from '../../../assets/img/tower/plant2.png';
import lamp from '../../../assets/img/tower/lamp.png';
import image from '../../../assets/img/tower/floor_plan.png';
import smiley from '../../../assets/img/tower/smily_image.png';
import sign_blue from '../../../assets/img/tower/sign_blue.png';
import sign_green from '../../../assets/img/tower/sign_green.png';
import sign_red from '../../../assets/img/tower/sign_red.png';
import lamp_entrance from '../../../assets/img/tower/lamp_entrance.png';
import door1 from '../../../assets/img/tower/door1.png';
import door2 from '../../../assets/img/tower/door2.png';
import door3 from '../../../assets/img/tower/door3.png';


export const images: {[index: string]: string} = {
  plant,
  plant2,
  lamp,
  image,
  smiley,
  sign_blue,
  sign_green,
  sign_red,
  lamp_entrance,
  door1,
  door2,
  door3
};
