import React from 'react';
import { StyledWalkable } from "./Styles";
import { useCharacter } from "../../../connected/character/store";

type Props = {
  floor: number;
}

const Walkable: React.FC<Props> = ({ floor }) => {
  const [ , setCharacter ] = useCharacter();
  const target = (e: React.MouseEvent<HTMLElement>) => ({
    floor: floor,
    x: e.nativeEvent.offsetX
  });

  return (
    <StyledWalkable
      id={'walkable_area ' + floor}
      floor={ floor }
      onClick={ (e) => setCharacter.setTarget( 1, target( e ) ) }
    />
  )
};

export default Walkable
