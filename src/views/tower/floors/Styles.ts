import styled from "styled-components";
import { colors } from "../../colors/colors";

interface FloorInterface {
  floor: number;
  color: string;
}

const getGridPosition = (floor: number) =>
  `floor${floor}_tile0-start / floor${floor}_tile8-end`;

export const StyledWalkable = styled.div<{floor: number}>`
position:relative;
  grid-row: ${props => getGridPosition(props.floor)};
  grid-column: ${props => getGridPosition(props.floor)};
  background-color: transparent;
  z-index: 15;
`;

export const StyledFloor = styled.div<FloorInterface>`
  grid-row: ${props => getGridPosition(props.floor)};
  grid-column: ${props => getGridPosition(props.floor)};
  background-color: ${ props => colors[props.color] };
  box-shadow: inset 18px 13px ${ props => colors[props.color + '_shadow'] };
`;

export const StyledShadowOverlay = styled.div<FloorInterface>`
  position: relative;
  grid-row: ${props => getGridPosition(props.floor)};
  grid-column: ${props => getGridPosition(props.floor)};
  background-color: ${ props => colors[props.color + '_shadow'] };
  top: -10px;
  height: 23px;
  z-index: 1;
`;

export const StyledFloorOverlay = styled.div<{ floor: number }>`
  position: relative;
  grid-row: ${props => getGridPosition(props.floor)};
  grid-column: ${props => getGridPosition(props.floor)};
  background-color: #6b2c29;
  border-top: 3px solid black;
  border-bottom: 3px solid black;
  bottom: 10px;
  height: 10px;
  z-index: 2;
`;

export const StyledSign = styled.div<{ floor: number }>`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  font-weight: bold;
  grid-row: ${props => getGridPosition(props.floor)};
  grid-column: ${props => getGridPosition(props.floor)};
  background-color: lightgray;
  top: 25px;
  left: -40px;
  height: 30px;
  width: 30px;
  z-index: 2;
`;