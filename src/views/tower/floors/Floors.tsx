import React, { Fragment, useRef } from 'react';
import { floor_colors } from "../tower";
import TileMapper from "../tiles/TileMapper";
import {
  StyledFloor,
  StyledFloorOverlay,
  StyledShadowOverlay,
  StyledSign
} from "./Styles";
import Walkable from "./Walkable";
import { RoomConstructor } from "../../../connected/rooms/room_constructor/RoomConstructor";

const rooms = new RoomConstructor();

export type Floors = Array<string | number>;

const mapFloors = () => {
  return rooms.list.map( (floor, i) => {
    const thisFloor = 12 - i;
    return (
      <Fragment key={ i } >

        <StyledFloor
          id={ 'floor_area ' + thisFloor }
          floor={ thisFloor }
          color={ floor_colors[i] }
        />
        <StyledShadowOverlay
          id={ 'shadow ' + thisFloor }
          floor={ thisFloor }
          color={ floor_colors[i] }
        />
        <StyledFloorOverlay
          id={ 'floor ' + thisFloor }
          floor={ thisFloor }
        />
        <StyledSign
          id={ 'sign ' + thisFloor }
          floor={ thisFloor }
        >
          { thisFloor }
        </StyledSign>
        <Walkable
          floor={ thisFloor }
        />
        <TileMapper
          floor={ floor }
        />
      </Fragment>
    )
  } )
};

const Floors: React.FC = () => {
  const floors = useRef( mapFloors() );

  return (
    <Fragment>
      { floors.current }
    </Fragment>
  );
};

export default Floors
