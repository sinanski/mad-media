type Tower = Array<string | 0>
// type Tower = Array<Room | 0>

// interface Tower {
//   [index:number]: string | number;
// }

export const floor_colors: Array<string> = [
  'green',
  'blue',
  'gray',
  'blue',
  'green',
  'gray',
  'green',
  'blue',
  'gray',
  'green',
  'blue',
  'green',
  'gray'
];

export const clickable = [
  1, 0, 0, 0, 0, 0, 1, 0,
  0, 0, 1, 0, 1, 0, 0, 0,
  1, 0, 1, 0, 1, 0, 1, 0,
  1, 0, 1, 0, 0, 0, 1, 0,
  0, 0, 1, 0, 1, 0, 0, 0,
  1, 0, 1, 0, 1, 0, 1, 0,
  1, 0, 0, 0, 0, 0, 1, 0,
  0, 0, 1, 0, 1, 0, 0, 0,
  1, 0, 1, 0, 1, 0, 1, 0,
  0, 0, 0, 0, 1, 0, 1, 0,
  0, 0, 1, 0, 1, 0, 0, 0,
  1, 0, 0, 0, 0, 0, 1, 0,
  1, 0, 0, 0, 0, 0, 1, 0,
];

export const sprites = [
  1, 0, 0, 0, 0, 0, 1, 0,
  0, 0, 1, 0, 1, 0, 0, 0,
  1, 0, 1, 0, 1, 0, 1, 0,
  1, 0, 1, 0, 0, 0, 1, 0,
  0, 0, 1, 0, 1, 0, 0, 0,
  1, 0, 1, 0, 1, 0, 1, 0,
  1, 0, 0, 0, 0, 0, 1, 0,
  0, 0, 1, 0, 1, 0, 0, 0,
  1, 0, 1, 0, 1, 0, 1, 0,
  0, 0, 0, 0, 1, 0, 1, 0,
  0, 0, 1, 0, 1, 0, 0, 0,
  1, 0, 0, 0, 0, 0, 1, 0,
  1, 0, 0, 0, 0, 0, 1, 0,
];

export const decorations: Tower = [
  0, 0, 'plant', 0, 0, 'plant2', 'lamp', 'image',
  'lamp', 'plant', 0, 0, 0, 0, 'smiley', 0,
  0, 'sign_blue', 0, 'sign_blue', 0, 'sign_blue', 0, 'sign_blue',
  0, 0, 0, 0, 'lamp', 'plant2', 0, 0,
  0, 'plant', 0, 0, 0, 0, 0, 0,
  0, 'sign_green', 0, 'sign_green', 0, 'sign_green', 0, 'sign_green',
  0, 0, 'lamp', 0, 'plant', 0, 0, 0,
  0, 'plant2', 0, 0, 0, 0, 'lamp', 'image',
  0, 'sign_red', 0, 'sign_red', 0, 'sign_red', 0, 'sign_red',
  'image', 'plant2', 'lamp', 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 'plant', 'image',
  0, 'plant', 'lamp', 0, 0, 'lamp', 'smiley', 0,
  0, 0, 0, 'lamp_entrance', 'lamp_entrance', 0, 0, 0
];


export const tower_rooms: Tower = [
  'betty', 0, 0, 0, 0, 0, 'bertram', 0,
  0, 0, 'real_estate', 0, 'peace_brothers', 0, 0, 0,
  'office_sun', 0, 'news_sun', 0, 'boss_sun', 0, 'archive_sun', 0,
  'commercials', 0, 'free_studio', 0, 0, 0, 'gun_agency', 0,
  0, 0, 'free_studio_2', 0, 'consulat_duban', 0, 0, 0,
  'office_fun', 0, 'news_fun', 0, 'boss_fun', 0, 'archive_fun', 0,
  'film_agency', 0, 0, 0, 0, 0, 'consulat_free_duban', 0,
  0, 0, 'free_studio_3', 0, 'screenplay', 0, 0, 0,
  'office_mad', 0, 'news_mad', 0, 'boss_mad', 0, 'archive_mad', 0,
  0, 0, 0, 0, 'tobacco_abuse', 0, 'tobacco_lobby', 0,
  0, 0, 'psychiatry', 0, 'bio_control', 0, 0, 0,
  'laundry', 0, 0, 0, 0, 0, 'super_market', 0,
  // 'usher', 0, 0, 0, 0, 0, 'floor_plan', 0,
  'usher', 'floor_plan', 0, 0, 0, 0, 0, 0,
];
