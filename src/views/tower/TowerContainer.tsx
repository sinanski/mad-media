import React, {Fragment} from "react";
import Elevator from "../../connected/elevator";
import Floors from "./floors/Floors";
import Character from '../../connected/character';
import { floor_colors } from "./tower";
import { Grid } from "./Styles";
import {config} from "../../config";
// import {createGrid} from "./HELPER_create_grid";
//
// createGrid();

export interface Location {
  is_moving?: boolean;
  floor: number;
  direction?: string;
  tile: number;
  id: string;
  x: number
}

const TowerContainer = () => {

  return (
    <Grid id='Tower-Grid'>
      <Floors />

      <Elevator floors={ floor_colors } />
      <Character characterIndex={ 1 } />
      { config.npcs &&
      <Fragment>
        <Character characterIndex={ 2 } isNPC={ true } />
        < Character characterIndex={ 3 } isNPC={ true } />
      </Fragment>
      }
    </Grid>
  );
};

export default TowerContainer;
