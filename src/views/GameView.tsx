import React from 'react';
import styled from 'styled-components';
import World from "./world/World";
import RoomContainer from "./tower/rooms/RoomContainer";
import {config} from "../config";
import RoomProvider from "../connected/rooms";

const Container = styled.div`
  display: grid;
  height: 100vh;
  width: 100vw;
  overflow: hidden;
  grid-template-rows: auto 150px;
`;

const UI = styled.div`
  display: flex;
  grid-row-start: 2;
  grid-column-start: 1;
  grid-column-end: 2;
  background-color: white;
`;

const GameView = () => (
  <Container id="Game">
    <World />
    <RoomProvider />
    <RoomContainer />
    { config.show_ui &&
      <UI id="UI" />
    }
  </Container>
);

export default GameView;
