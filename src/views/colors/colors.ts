export const green = '#71ad66';
export const green_shadow = '#1d923a';
export const blue = '#82c1d7';
export const blue_shadow = '#427da6';
export const gray = '#9b9b99';
export const gray_shadow = '#656565';
export const brown = '#2b120f'

export const colors: { [index: string]: string } = {
  green,
  blue,
  gray,
  green_shadow,
  blue_shadow,
  gray_shadow,
  brown
}