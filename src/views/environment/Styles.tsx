import styled from "styled-components";
import wallImage from "../../assets/img/tower/wall.png";

export const Wall = styled.div<{area: string}>`
  grid-area: ${props => props.area};
  border-right: 4px solid black;
  border-left: 4px solid black;
  background-image: url('${wallImage}');
  background-repeat: repeat;
  background-size: 40px;
`;

export const Roof = styled.img`
  position: relative;
  grid-area: Roof;
  border-bottom: 4px solid black;
  width: 100%;
  bottom: 4px;
  z-index: 10;
`;

export const Logo = styled.img`
  position: relative;
  left: 50%;
  transform: translateX(-50%);
  bottom: -10px;
  grid-area: Logo;
  z-index: 11;
`;

export const OuterBuilding = styled.img`
  position: absolute;
  bottom: 0;
  left: -100px;
  width: 100px;
  height: 100px;
  grid-area: Wall-left;
  z-index: 11;
`;

export const Fence = styled.img`
  position: absolute;
  bottom: 0;
  right: -268px;
  width: 270px;
  height: 65px;
  grid-area: Wall-right;
  z-index: -1;
`;
