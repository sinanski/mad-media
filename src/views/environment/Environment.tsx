import React, {Fragment} from 'react';
import {
  Roof,
  Logo,
  Wall,
  Fence,
  OuterBuilding
} from "./Styles";
import roofImage from "../../assets/img/tower/roof.png";
import logo from "../../assets/img/tower/mad-tv.png";
import outerBuilding from "../../assets/img/tower/outer-building.png";
import fence from "../../assets/img/tower/fence.png";


const Environment = () => (
  <Fragment>
    <Roof src={roofImage} id="roof" />
    <Logo src={logo} id="logo" />
    <Wall area='Wall-left' />
    <Wall area='Wall-right' />
    <OuterBuilding src={outerBuilding} />
    <Fence src={fence} id="fence" />
  </Fragment>
);

export default Environment
