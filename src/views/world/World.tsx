import React from 'react';
import styled from 'styled-components';
import Tower from "../tower";
import Panorama from "./Panorama";
import Environment from "../environment/Environment";
import { config } from "../../config";

const Container = styled.div`
  display: flex;
  grid-row-start: 1;
  grid-row-end: ${() => config.show_ui ? 2 : 3};
  grid-column-start: 1;
  grid-column-end: 2;
  overflow-x: hidden;
  overflow-y: scroll;
  width: 100%;
  justify-content: center;
`;

const OuterGrid = styled.div`
  position:relative;
  display: grid;
  grid-template-columns: 50px auto 50px;
  grid-template-rows: 190px 32px auto;
  grid-template-areas:
  ". Logo ."
  "Roof Roof Roof"
  "Wall-left Tower Wall-right"
`;

const World = () => {
  return (
    <Container id="World">
      <Panorama
        isNight={ false }
      />
      <OuterGrid id='Outer-Grid'>
        <Environment />
        <Tower />
      </OuterGrid>
    </Container>
  );
};

export default World
