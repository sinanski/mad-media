import React from 'react';
import { keyframes } from 'styled-components';
import {
  Container,
  SkyDay,
  SkyEvening,
  SkyNight,
  Stars,
  Skyline,
  SkylineImage
} from "./Styles";
import skylineImg from "../../assets/img/tower/skyline.png";

// Problem hier ist, dass das Background Image flackert.
// Wenn ich ein Image reinziehe flackert es nciht,  lässt sich aber nciht so leicht auf repeat stellen.

export interface Night {
  isNight: boolean;
  duration: number;
  delay: number;
  animate: {};
}

interface Props {
  isNight: boolean;
}

const halveOpacity = keyframes`
  from { opacity: 0;}
  to { opacity: .4;}
`;

const fullOpacity = keyframes`
  from { opacity: 0;}
  to { opacity: 1;}
`;

const Panorama: React.FC<Props> = ({ isNight }) => {

  return (
    <Container id='Background'>
      <SkyDay id="Sky" />
      <SkyEvening
        id="Sky Evening"
        isNight={ isNight }
        duration={ 2 }
        delay={ 2 }
        animate={ halveOpacity }
      />
      <SkyNight
        id="Sky Night"
        isNight={ isNight }
        duration={ 10 }
        delay={ 3 }
        animate={ fullOpacity }
      />
      <Stars
        id="Stars"
        isNight={ isNight }
        duration={ 10 }
        delay={ 5 }
        animate={ fullOpacity }
      />

      {/* This is not a prefered solution. But setting the image as css background causes flickering */}
      <Skyline id='Skyline'>
        <SkylineImage src={skylineImg} alt="" />
        <SkylineImage src={skylineImg} alt="" />
        <SkylineImage src={skylineImg} alt="" />
        <SkylineImage src={skylineImg} alt="" />
      </Skyline>
    </Container>
  );
};

export default Panorama;
