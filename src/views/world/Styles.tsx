import styled, { css } from "styled-components";
import sky from "../../assets/img/tower/sky.png";
import skylineImg from "../../assets/img/tower/skyline.png";
import { Night } from "./Panorama";

const animation = (props: Night) =>
  props.isNight ?
  css`
    ${ props.animate } ${ props.duration }s linear forwards ${ props.delay }s;
  `
  : ''
;

const Absolute = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

export const Container = styled(Absolute)`
  background-color: black;
  z-index: -1;
`;

export const SkyDay = styled(Absolute)`
  background: rgb(67,186,237);
  background: linear-gradient(180deg, rgba(67,186,237,1) 0%, rgba(232,246,252,1) 100%);
  z-index: -4;
`;

export const SkyEvening = styled(Absolute)<Night>`
  background: red;
  opacity: 0;
  animation: ${ animation };
  z-index: -3;
`;

export const SkyNight = styled(Absolute)<Night>`
  background: black;
  opacity: 0;
  animation: ${ animation };
  z-index: -2;
`;

export const Stars = styled(Absolute)<Night>`
  background-image: url('${ sky }');
  background-repeat: repeat no-repeat;
  background-size: 60%;
  opacity: 0;
  animation: ${ animation };
  z-index: -1;
`;

export const Skyline = styled(Absolute)`
display: flex;
  background:
  // url('${ skylineImg }'),
  linear-gradient(0deg, rgba(69,68,66,1) 0%, rgba(69,68,66,1) 35%, rgba(69,68,66,0) 35%);
  height: 260px;
  background-repeat: no-repeat;
  //background-size: 38%;
`;

export const SkylineImage = styled.img`
  align-self: flex-start;
  width: 420px;
`;
