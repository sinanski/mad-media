import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { Draggable } from "react-beautiful-dnd";

interface Props {
  id: string;
  ElementNode: React.FC;
  index: number;
  element: any;
}

const Container = styled.div`
//  background-color: { props => props.isDragging ? 'lightgreen' : 'white' };
`;

class Element extends PureComponent<Props> {
  render() {
    const {
      element,
      id,
      index,
      ElementNode
    } = this.props;
    return (
      <Draggable draggableId={ id } index={ index }>
        { (provided, snapshot) => (
          <Container
            { ...provided.draggableProps }
            { ...provided.dragHandleProps }
            ref={ provided.innerRef }
            isDragging={ snapshot.isDragging }
          >
            <ElementNode
              { ...element }
              isDragging={ snapshot.isDragging }
            >
            </ElementNode>
          </Container>
        ) }
      </Draggable>
    )
  }
}

export default Element
