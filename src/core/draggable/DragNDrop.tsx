import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { DragDropContext } from "react-beautiful-dnd";
import Column from "./Column";
import { onDragEnd, hasNoChange } from "./onDragEnd";
import { State, DragEnd } from "./interfaces";
import { DropResult } from "react-beautiful-dnd";

interface Props {
  ColumnNode: Array<React.FC>;
  // todo no idea why React.FC is not accepted here.
  ElementNode: any;
  data: State;
  onDragEnd: DragEnd
}

const Container = styled.div`
  display: flex;
`;

class DragNDrop extends PureComponent<Props> {

  state = this.props.data;

  onDragEnd = (result: DropResult) => {
    if ( !hasNoChange( result ) ) {
      const newState = onDragEnd( result, this.state );
      this.setState( newState );
      this.props.onDragEnd( result, this.state )
    }
  };

  render() {
    const { columnOrder, columns, elements } = this.state;
    return (
      <Container>
        <DragDropContext onDragEnd={ this.onDragEnd }>
          { columnOrder.map( (columnId, i) => {
            const column = columns[columnId];
            const elem = column.elementIds.map(
              elementId => elements[elementId]
            );
            return (
              <Column
                ColumnNode={ this.props.ColumnNode[i] }
                ElementNode={this.props.ElementNode}
                key={ columnId }
                column={ column }
                elements={ elem }
              />
            );
          } ) }
        </DragDropContext>
      </Container>
    )
  }
}

export default DragNDrop
