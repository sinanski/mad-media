// expects [{id: 'objectId', ...rest}]
// returns {objectId: {id: objectId, ...rest}}

interface Returned {
  [index: string]: Base
}

interface Base {
  id: string;
  [index: string]: any;
}

export function dataConverter(data: Array<Base>) {
  let result: {[index: string]: any} = {};
  data.forEach( single => (
    result[single.id] = single
  ));
  return result
}

// const x = dataConverter( [ { id: 'test', foo: 'bar' } ] );
// console.log(x)
