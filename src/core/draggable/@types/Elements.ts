export interface ColumnData {
  [index: string]: {
    id: string;
    elementIds: Array<string>;
    [index: string]: any;
  }
}

export interface ElementData {
  [index: string]: {
    id: string;
    title?: string;
    [index: string]: any;
  }
}
