import { Source, State } from "./interfaces";
import { DropResult } from "react-beautiful-dnd";

export function onDragEnd(result: DropResult, state: State): State {
  if ( isSameColumn(result, state) ) {
    return reorderInSameColumn(result, state);
  }
  return reorderInDifferentColumn(result, state)
}

export function hasNoChange(result: DropResult): Boolean {
  const { source, destination } = result;
  if ( !destination ) {
    return true;
  }
  return !!(destination.droppableId === source.droppableId &&
    destination.index === source.index);
}

function isSameColumn(result: DropResult, state: State) {
  const { source, destination } = result;
  if ( !destination ) return true;
  const start = state.columns[source.droppableId];
  const finish = state.columns[destination.droppableId];
  return start === finish;
}

function reorderInSameColumn(result: DropResult, state: State) {
  const newColumn = getNewColumn(result, state);
  return {
    ...state,
    columns: {
      ...state.columns,
      [newColumn.id]: newColumn
    }
  };
}

function reorderInDifferentColumn(result: DropResult, state: State) {
  const { source } = result;
  const newStart = getNewStart(state, source);
  const newFinish = getNewFinished(result, state);
  return {
    ...state,
    columns: {
      ...state.columns,
      [newStart.id]: newStart,
      [newFinish.id]: newFinish,
    }
  };
}

function getNewColumn(result: DropResult, state: State) {
  const { source, destination, draggableId } = result;
  const start = state.columns[source.droppableId];
  const newElementIds = Array.from(start.elementIds);
  newElementIds.splice(source.index, 1);
  newElementIds.splice(destination!.index, 0, draggableId);
  return {
    ...start,
    elementIds: newElementIds
  };
}

function getNewStart(state: State, source: Source) {
  const start = state.columns[source.droppableId];
  const startElementIds = Array.from(start.elementIds);
  startElementIds.splice(source.index, 1);
  return {
    ...start,
    elementIds: startElementIds
  };
}

function getNewFinished(result: DropResult, state: State) {
  const { destination, draggableId } = result;
  const finish = state.columns[destination!.droppableId];
  const finishElementIds = Array.from(finish.elementIds);
  finishElementIds.splice(destination!.index, 0, draggableId);
  return {
    ...finish,
    elementIds: finishElementIds
  };
}
