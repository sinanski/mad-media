import React from "react";
import DragNDrop from "./DragNDrop";
import { ColumnData } from "./@types/Elements";
import { State } from "./interfaces";
import { dataConverter } from "./dataConverter";
import { DropResult } from "react-beautiful-dnd";

type DragEnd = (result: DropResult, state: State) => void;
type ColumnNode =  React.FC | Array<React.FC>;

interface Props {
  ColumnNode: ColumnNode;
  columnData: ColumnData;
  // todo no idea why React.FC is not accepted here.
  ElementNode: any;
  elementData: Array<{
    id: string;
    [index: string]: any;
  }>;
  onDragEnd?: DragEnd;
}

function getData(props: Props): State {
  const elements = dataConverter( props.elementData );
  const columns = props.columnData;
  const columnOrder = getColumnOrder( columns )
  return {
    elements,
    columns,
    columnOrder
  };
}

function getColumnOrder(columns: {}): Array<string> {
  return Object.keys( columns )
    .map( columnId => (
      columnId
    ) );
}

const dummy = () => {};

function columnNodeToArray(props: Props) {
  let nodes: Array<React.FC> = [];
  nodes = nodes.concat( props.ColumnNode );
  const data = getData(props);
  return Object.keys(data.columnOrder).map((a, i) => (
    nodes[i] || nodes[0]
  ))
}

const DragNDropProvider = (props: Props) => {
  return (
    <DragNDrop
      { ...props }
      ColumnNode={columnNodeToArray(props)}
      onDragEnd={ props.onDragEnd || dummy }
      data={ getData( props ) }
    />
  );
}

export default DragNDropProvider
