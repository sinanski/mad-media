import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { Droppable } from "react-beautiful-dnd";
import Element from "./Element";
import { ColumnData } from "./@types/Elements";

const Title = styled.h3`
  padding: 8px;
`;

const ElementList = styled.div`
  padding: 8px;
// @ts-ignore
  background-color: ${ props => props.isDraggingOver ? '#0000000d' : 'transparent' };
  transition: background-color .3s ease;
`;

interface InnerListProps {
  elements: Array<{
    id: string
  }>;
  ElementNode: React.FC;

}

class InnerList extends PureComponent<InnerListProps> {
  render() {
    return this.props.elements.map( (element, index) => (
      <Element
        id={ element.id }
        ElementNode={this.props.ElementNode}
        key={ element.id }
        element={ element }
        index={ index }
      />
    ) )
  }
}

interface Props {
  ColumnNode: React.FC;
  ElementNode: React.FC;
  column: ColumnData;
  elements: any;
}

export default class Column extends PureComponent<Props> {
  render() {
    const { column, elements, ColumnNode, ElementNode } = this.props;
    return (
      <ColumnNode {...this.props}>
        <Title>
          { column.title }
        </Title>
        <Droppable
          //@ts-ignore
          droppableId={ column.id }
        >
          { (provided, snapshot) => (
            <ElementList
              { ...provided.droppableProps }
              ref={ provided.innerRef }
              isDraggingOver={ snapshot.isDraggingOver }
            >
              <InnerList
                ElementNode={ ElementNode }
                elements={ elements }
              />
              { provided.placeholder }
            </ElementList>
          ) }
        </Droppable>
      </ColumnNode>
    )
  }
}
