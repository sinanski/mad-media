import { initialData } from "./initialData";
import { DropResult } from "react-beautiful-dnd";

export interface Source {
  "index": number,
  "droppableId": string
}

export interface Destination {
  "droppableId": string,
  "index": number
}

export interface Result {
  "draggableId": string,
  "type": string,
  "source": Source,
  "destination": Destination,
  // "reason": string,
  // "mode": "FLUID",
  // "combine": null
}

export type State = typeof initialData;

export type DragEnd = (result: DropResult, state: State) => void;
