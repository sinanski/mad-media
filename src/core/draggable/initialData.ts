import { ColumnData, ElementData } from "./@types/Elements";


interface State {
  elements: ElementData;
  columns: ColumnData;
  columnOrder: Array<string>;
}


export const initialData: State = {
  elements: {
    'task-1': { id: 'task-1', content: '1. Film' },
    'task-2': { id: 'task-2', content: '2. Film' },
    'task-3': { id: 'task-3', content: '3. Film' },
    'task-4': { id: 'task-4', content: '4. Film' },
  },
  columns: {
    'column-1': {
      id: 'column-1',
      title: 'Filme',
      elementIds: [ 'task-1', 'task-2', 'task-3', 'task-4' ]
    },
    'column-2': {
      id: 'column-2',
      title: 'Programm',
      elementIds: []
    },
    'column-3': {
      id: 'column-3',
      title: 'Müll',
      elementIds: []
    },
  },
  columnOrder: ['column-1', 'column-2', 'column-3']
};
