export interface Ad {
  id: string;
  title: {
    de: string;
    en?: string;
  };
  description: {
    de: string;
    en?: string;
  };
  conditions: {
    min_audience: string;
    min_image: string;
    target_group?: string;
    pro_pressure_groups?: string;
    contra_pressure_groups?: string;
    allowed_programme_type?: string;
    prohibited_programme_type?: string;
  };
  data: {
    quality: string;
    repetitions: string; // how often it needs to be aired
    duration: string; // the amount of days till penalty?
    profit: string;
    penalty: string;
    infomercial_profit?: string;
    fix_infomercial_profit?: string;
    infomercial?: string;
    year_range_from?: string;
    year_range_to?: string;
  }
}
