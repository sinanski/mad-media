import { get } from 'lodash';
import ads from '../../constants/ads.json';
import { Ad } from "./AdInterface";

export default class Contract {
    get = ( contracts: Array<string> ) => filterIds(contracts);
    random = random;
    sortByName = (contracts: Array<string>) => byName(contracts);
    sortByMinAudience = (contracts: Array<string>) => byMinAudience(contracts);
    sortByProfit = (contracts: Array<string>) => byProfit(contracts);
}

function random(amount: number): Array<Ad> {
  let list = [];
  for ( let i = 0; i<amount;i++ ) {
    const rand = Math.round(Math.random() * ads.length);
    list.push(ads[rand]);
  }
  return list
}

function byName(ids: Array<string>): Array<Ad> {
  // todo this is currently not working
  return filterIds(ids)
    .sort(sortBy('title.de'));
}

function byMinAudience(ids: Array<string>): Array<Ad> {
  return filterIds(ids)
    .sort(sortBy('conditions.min_audience'));
}

function byProfit(ids: Array<string>): Array<Ad> {
  return filterIds(ids)
    .sort(sortBy('data.profit'));
}

function filterIds(ids: Array<string>): Array<Ad> {
  return ads.filter( ad =>
    ids.indexOf( ad.id ) !== -1
  );
}

function sortBy(key: string) {
  return function(a: Ad, b: Ad) {
    const aa = convertNumbers(a, key);
    const bb = convertNumbers(b, key);
    if(aa < bb) { return -1; }
    if(aa > bb) { return 1; }
    return 0;
  }
}

function convertNumbers(ad: Ad, key: string): string | number {
  let result = get(ad, key, 0);
  if ( isNumber(result) ) {
    result = Number(result)
  }
  return result
}

function isNumber(val: string | number): boolean {
  return !isNaN(Number(val))
}