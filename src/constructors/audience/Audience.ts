import people from '../../constants/people.json';
import programs from '../../constants/programs.json'
import programDataMods from '../../constants/programDataMods.json'

interface AudienceInterface {

}

console.log(people)
console.log(programs)
console.log(programDataMods)

const defaultBreakdown: { [index: string]: number } = {
  children: 0.09, // 'Kinder (9%)
  teenagers: 0.1, // Teenager 10% adults 60%
  houseWives: 0.12, // 'Hausfrauen (20% von 60% Erwachsenen = 12%)
  employees: 0.405, // 'Arbeitnehmer (67,5% von 60% Erwachsenen = 40,5%)
  unemployed: 0.045, //'Arbeitslose (7,5% von 60% Erwachsenen = 4,5%)
  manager: 0.03, //  'Manager (5% von 60% Erwachsenen = 3%)
  pensioners: 0.21, //  'Rentner (21%)
};

const defaultGenderBreakdown: { [index: string]: number } = {
  children: 0.487, // 'Kinder (9%)
  teenagers: 0.487, // Teenager 10% adults 60%
  houseWives: 0.9, // 'Hausfrauen (20% von 60% Erwachsenen = 12%)
  employees: 0.4, // 'Arbeitnehmer (67,5% von 60% Erwachsenen = 40,5%)
  unemployed: 0.45, //'Arbeitslose (7,5% von 60% Erwachsenen = 4,5%)
  manager: 0.2, //  'Manager (5% von 60% Erwachsenen = 3%)
  pensioners: 0.58, //  'Rentner (21%)
};

export default class Audience {
  constructor(
    public currentAudienceBreakdown: AudienceInterface,
    public currentGenderBreakdown: AudienceInterface,
    public targetAudienceBreakdown:AudienceInterface,
    public targetGenderBreakdown: AudienceInterface,
    public defaultAudienceBreakdown: AudienceInterface,
    public defaultGenderBreakdown: AudienceInterface
  ) {

  }
};
