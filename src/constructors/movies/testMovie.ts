export const testMovie = {
  name: 'Test Film',
  description: 'Ein mittelmäßiger Film, der die Geduld testet',
  age: 16,
  blocks: 3,
  price: 5000,
  category: 'love', // comedy, crime, drama ...
  popularity: 13, // 1 - 100
  popularity_elder: 21,
  popularity_adults: 16,
  popularity_children: 7,
};
