export interface MovieInterface {
  name: string,
  description: string,
  age: number,
  blocks: number,
  price: number,
  category: string, // comedy, crime, drama ...
  popularity: number, // 1 - 100
  popularity_elder: number,
  popularity_adults: number,
  popularity_children: number,
}
