export const news = {
  politics_economy: {
    children: {
      value: -1.2
    },
    teenagers: {
      value: -0.5
    },
    houseWifes: {
      men: -0.2,
      women: -0.5,
    },
    employees: {
      man: 0,
      women: -0.15,
    },
    unemployed: {
      man: -0.1,
      women: -0.25,
    },
    manager: {
      man: 0.4,
      women: 0.2,
    },
    pensioners: {
      man: 0.15,
      women: 0,
    },
  },
  showbiz: {
    children: {
      value: -0.1
    },
    teenagers: {
      men: 0.2,
      women: 0.4,
    },
    houseWifes: {
      men: 0.1,
      women: 0.4,
    },
    employees: {
      man: 0,
      women: 0.15,
    },
    unemployed: {
      man: 0.15,
      women: 0.4,
    },
    manager: {
      man: -0.4,
      women: -0.2,
    },
    pensioners: {
      man: -0.1,
      women: 0,
    },
  },
  sport: {
    children: {
      value: 0
    },
    teenagers: {
      men: 0.3,
      women: 0.1,
    },
    houseWifes: {
      men: 0.1,
      women: 0,
    },
    employees: {
      man: 0.2,
      women: 0,
    },
    unemployed: {
      man: 0.2,
      women: -0.1,
    },
    manager: {
      man: 0.1,
      women: -0.4,
    },
    pensioners: {
      man: 0.1,
      women: 0,
    },
  },
  technics_media: {
    children: {
      value: 0
    },
    teenagers: {
      value: -0.25,
    },
    houseWifes: {
      men: 0.1,
      women: 0.2,
    },
    employees: {
      man: -0.05,
      women: 0.1,
    },
    unemployed: {
      man: -0.15,
      women: -0.1,
    },
    manager: {
      man: -0.25,
      women: -0.1,
    },
    pensioners: {
      man: 0.25,
      women: 0.4,
    },
  },
  culture: {
    children: {
      value: -0.5
    },
    teenagers: {
      value: -0.5,
    },
    houseWifes: {
      men: -0.6,
      women: -0.3,
    },
    employees: {
      man: 0,
      women: 0.1,
    },
    unemployed: {
      man: -0.6,
      women: -0.3,
    },
    manager: {
      man: 0.2,
      women: 0.3,
    },
    pensioners: {
      man: 0.3,
      women: 0.5,
    },
  },


};
