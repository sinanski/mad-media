import React, { useEffect } from 'react';
import { useCharacter } from "../connected/character/store";
import { RoomConstructor } from "../connected/rooms/room_constructor/RoomConstructor";
import useRooms from "../connected/rooms/store";
import { RoomInterface, RoomListInterface } from "../connected/rooms/actions/types";
import { usePrevious } from "../assets/functions/use/usePrevious";
import { Character } from "../connected/character/actions/types";

const getRooms = new RoomConstructor();

interface Props {
  index: number;
  id: string;
}

// todo should not be total random
// There should be a logic that keeps KI mainly on own floor
// And duration should depend on visited room. Highest duration in office.
const randomTarget = (id: string, rooms: RoomListInterface): RoomInterface => {
  const valid = getRooms.validRooms( id );
  const rand = Math.round( Math.random() * valid.length );
  const randomKey = Object.keys( rooms )[rand]
  return rooms[randomKey];
  // return rooms.laundry;
};

const KI: React.FC<Props> = (
  {
    index,
    id
  }
) => {
  const [ char, setCharacter ] = useCharacter();
  const [ rooms ] = useRooms();
  const character = char[index];
  const prevCharacter = usePrevious<Character>( character );

  useEffect( () => {
    const target = randomTarget( id, rooms );
    setCharacter.setTarget( index, target );
  }, [] ); // eslint-disable-line

  useEffect( () => {
    const target = randomTarget( id, rooms );
    const setTarget = () => setCharacter.setTarget( index, target );
    const hasArrived = character.has_arrived && prevCharacter && !prevCharacter.has_arrived;
    let timer: any;

    if ( hasArrived ) {
      const delay = (Math.random() * 5 + 3) * 1000;
      timer = setTimeout( () => {
        console.log( "Set Target" );
        setTarget();
        return () => clearTimeout( timer );
      }, delay );
    }

  }, [ character, prevCharacter ] ); // eslint-disable-line

  return null
};

export default KI
